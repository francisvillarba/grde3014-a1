/**
 * server.js.
 *
 * The main server js for GRDE3014, Web Authoring Design, Assignment 1.
 *
 * @author  Francis Villarba <francis.villarba@student.curtin.edu.au>
 * @version 1.0.1
 *
 * @requires nodejs
 * @requires mongodb
 * @requires express
 * @requires mongoose
 */

// -------------------------------------------------------------------------- //
// 1. Dependencies, Settings and Application Initialisation                   //
// -------------------------------------------------------------------------- //

const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const port = process.env.PORT || 8080;          // Fallback to 8080 if NULL
const ipa = process.env.IP;                     // The IP Address for the C9
const app = express();                          // Application Init

const router = express.Router();                // Routing for modularity

// -------------------------------------------------------------------------- //
// 2. Middleware and Plugins                                                  //
// -------------------------------------------------------------------------- //

// Setup the server's ability to receieve and process incoming JSON
app.use(bodyParser.urlencoded( {extended: false} ));
app.use(bodyParser.json());

// -------------------------------------------------------------------------- //
// 3. Database Connection                                                     //
// -------------------------------------------------------------------------- //

mongoose.connect(
    'mongodb://245bbf4c6e183d05388e5e916e275449:nPcxWLtx9WVUgT3TQEhZ3fcB@10a.mongo.evennode.com:27017/245bbf4c6e183d05388e5e916e275449',
    {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false
    }
)
.then( () => {
    console.log("Connected to EvenNode - A1");
})
.catch( err => {
    console.log('Problem connecting to MongoDB EvenNode Server. Exiting...', err);
    process.exit();
});

// -------------------------------------------------------------------------- //
// 4. Routing / URI Endpoints                                                 //
// -------------------------------------------------------------------------- //

/**
 * Modularisation of routes to seperate files using JavaScript Modules
 * https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/routes
 */

// Main Routes
// TODO Convert this to Router, Model View Concept
require('./routes/main')(app);                                     // Home Page

// Define the routes
const itemsRouter = require('./routes/items');
const orderRouter = require('./routes/order');
const usersRouter = require('./routes/users');

// Tell server to use these routes for those URIs
app.use('/api/items', itemsRouter);                                // For Items
app.use('/api/orders', orderRouter);                               // For Orders
app.use('/api/users', usersRouter);                                // For Users

// -------------------------------------------------------------------------- //
// 5. Server                                                                  //
// -------------------------------------------------------------------------- //

app.listen(port, () => {
    console.log(`Welcome to Francis Villarba's GRDE3014 - A1`);
    // console.log(`IP Address: ${ipa}`);
    // console.log(`Running on port ${port}`);
    console.log(`Running on: ${ipa}:${port}`);
});
