/**
 * main.js.
 *
 * The routing for website
 *
 * @author  Francis Villarba <francis.villarba@student.curtin.edu.au>
 * @version 0.1.0
 *
 * @requires nodejs
 * @requires express
 */

module.exports = function(app) {

    // Handling home / root domain HTTP GET Request
    app.get('/', (req, res) => {
        res.status(200).send('Hello World! Welcome to Francis\' WAD Assignment 1, Please use Postman for APIs and don\'t forget to check the README.md documentation!');
    });

}
