/**
 * users.js.
 *
 * The routing for the user api
 *
 * @author  Francis Villarba <francis.villarba@student.curtin.edu.au>
 * @version 1.1.5
 *
 * @requires nodejs
 * @requires express
 * @requires express.Router
 */

// Setup  ------------------------------------------------------------------- //
var express = require('express');
var router = express.Router();

// Require Controller Modules
var usersController = require('../controllers/usersController');

// Creation ----------------------------------------------------------------- //
router.get('/create', usersController.createUserForm);
router.post('/create', usersController.createUser);

// Listing ------------------------------------------------------------------ //
router.get('/', usersController.listUsers);
router.get('/:id', usersController.listUser);

// Verification ------------------------------------------------------------- //
router.get('/:id/verify/:pw', usersController.verifyLogin);

// Modification Calls ------------------------------------------------------- //
router.get('/:id/edit', usersController.editUserForm);
router.put('/:id/edit', usersController.editUser);
router.put('/:id/edit/password', usersController.changePassword);

// Deletion ----------------------------------------------------------------- //
router.get('/:id/delete', usersController.deleteUserForm);
router.delete('/:id/delete', usersController.deleteUser);
router.delete('/:id/delete/:pw', usersController.deleteUserVerified);

module.exports = router;
