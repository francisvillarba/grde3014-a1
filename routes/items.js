/**
 * items.js.
 *
 * The routing for the items api
 *
 * @author  Francis Villarba <francis.villarba@student.curtin.edu.au>
 * @version 1.1.5
 *
 * @requires nodejs
 * @requires express
 * @requires router
 */

// Setup  ------------------------------------------------------------------- //
var express = require('express');
var router = express.Router();

// Item controller and methods
var itemsController = require('../controllers/itemsController');

// Item Creation ------------------------------------------------------------ //
router.get('/create', itemsController.createItemView);
router.post('/create', itemsController.createItemPost);

// Listing ------------------------------------------------------------------ //
router.get('/', itemsController.getItems);
router.get('/:id', itemsController.getItem);

// Modification Calls ------------------------------------------------------- //

// Edit
router.get('/:id/edit', itemsController.editItemView);
router.put('/:id/edit', itemsController.editItemPost);

// Add
router.put('/:id/add/tag/', itemsController.addToArray);
router.put('/:id/add/names/', itemsController.addToArray);

// Remove
router.put('/:id/remove/tag/', itemsController.pullFromArray);
router.put('/:id/remove/names/', itemsController.pullFromArray);
// Use put as we are editing not creating / deleting documents

// Item Deletion ------------------------------------------------------------ //
router.get('/:id/delete', itemsController.deleteItemGet);
router.delete('/:id/delete', itemsController.deleteItem);

module.exports = router;
