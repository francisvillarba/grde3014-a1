/**
 * order.js.
 *
 * The routing for the order api
 *
 * @author  Francis Villarba <francis.villarba@student.curtin.edu.au>
 * @version 1.1.5
 *
 * @requires nodejs
 * @requires express
 * @requires router
 */

// Setup  ------------------------------------------------------------------- //
var express = require('express');
var router = express.Router();

// Require controller Modules
var ordersController = require('../controllers/ordersController');

// Creation ----------------------------------------------------------------- //
router.get('/create', ordersController.createOrderView);
router.post('/create', ordersController.createOrder);

// Listing ------------------------------------------------------------------ //
router.get('/', ordersController.getOrders);
router.get('/:id', ordersController.getOrder);

// Modification Calls ------------------------------------------------------- //
router.get('/:id/edit', ordersController.editOrderView);
router.put('/:id/edit', ordersController.editOrder);
// use put as we are just editing the order itself

// Modification - Items
router.post('/:id/add/item', ordersController.addItem);
router.delete('/:id/delete/item/:item', ordersController.removeItem);
// use post and delete as we are creating and deleting OrderItems documents.

// Modification - Notes
router.post('/:id/add/note/:item', ordersController.addItemNote);
router.delete('/:id/delete/note/:item/:note', ordersController.removeItemNote);
router.delete('/:id/delete/notes/:item', ordersController.clearItemNotes);
// Use delete and post as we are creating and deleting OrderNotes documents.

// Deletion ----------------------------------------------------------------- //
router.get('/:id/delete', ordersController.deleteOrderView);
router.delete('/:id/delete', ordersController.deleteOrder);

module.exports = router;
