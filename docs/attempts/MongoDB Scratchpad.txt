/ ---------------------------------------------------------------------------- \
    MongoDB Example Queries and Scratchpad
    Written by Francis Villarba
    For GRDE3014 - Assignment 1
\ ---------------------------------------------------------------------------- /

////////////////////////////////////////////////////////////////////////////////
Attemps to Query an Array of Arrays in MongoDB using Mongo Shell
\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

// WORKING | Getting items based on object_id of the OrderItems document
db.orders.find( { "items._id": ObjectId("5e7b03addb1b7a4ca48b4bc5") }).pretty()


// SCRATCHPAD & TRIALS  - Getting items based on the object_id of the OrderItems OrderNotes document
db.orders.find({ 'items.notes':{ $elemMatch:{ $elemMatch:{ '_id': ObjectId("5e7b03addb1b7a4ca48b4bc7") } } } })
db.multiArr.find({ 'notes':{ $elemMatch:{ $elemMatch:{ '_id': ObjectId("5e7b03addb1b7a4ca48b4bc7") }}} })
db.orders.find({ 'items.notes': { '_id': ObjectId("5e7b03addb1b7a4ca48b4bc7") } })
db.orders.find({ 'items': { 'notes._id': ObjectId("5e7b03addb1b7a4ca48b4bc7") } })

db.orders.find({ 'items.notes': {
    '$all': [{
        '_id': ObjectId("5e7b03addb1b7a4ca48b4bc7")
    ]}
}})

// WORKING | Getting items based on the Object_Id of the OrderItems OrderNotes document
db.orders.find({ 'items.notes': { $elemMatch: { '_id': ObjectId("5e7b03addb1b7a4ca48b4bc7") } } }).pretty()

// SCRATCHPAD & TRIALS - Pull a given note
db.orders.update({}, { $pull: { 'items.notes': { $elemMatch: { '_id': ObjectId("5e7b03addb1b7a4ca48b4bc7")} } } })
db.orders.update( { 'items.notes': { $elemMatch: { '_id': ObjectId("5e7b03addb1b7a4ca48b4bc7")}}}, { $pull: { 'notes': { $elemMatch: { '_id': ObjectId("5e7b03addb1b7a4ca48b4bc7") }}}})
db.orders.findAndModify( { 'items.notes': { $elemMatch: { '_id': ObjectId("5e7b03addb1b7a4ca48b4bc7") }}}, update: { $pull: { 'notes': { $elemMatch: { '_id': ObjectId("5e7b03addb1b7a4ca48b4bc7") }}}})

// WORKING | Pull a given note
db.orders.update( { 'items.notes': { $elemMatch: { '_id': ObjectId("5e7b03addb1b7a4ca48b4bc7") } } }, { $pull: { 'items.$[].notes': { '_id': ObjectId("5e7b03addb1b7a4ca48b4bc7") } } } )

// SCRATCHPAD & TRIALS - Add a note to a given Order's Item


// Check if an order and item exists
db.orders.find({'_id': ObjectId("5e7b03addb1b7a4ca48b4bc4")})
db.orders.find({'items._id': ObjectId("5e7b03addb1b7a4ca48b4bc6")})

// WORKING | Add a note to a given Order's Item_Id
db.orders.update(
    {
        '_id': ObjectId("5e7b03addb1b7a4ca48b4bc4"),
        'items._id': ObjectId("5e7b03addb1b7a4ca48b4bc6")
    },
    {
        $push: {
            'items.$.notes': {
                "flag": 3,
                "comment": "This is a new note!"
            }
        }
    }
)

// WORKING | Get Items & notes Object_Ids
db.orders.find({ "_id": ObjectId("5e7b03addb1b7a4ca48b4bc4") }, { "items.notes._id": 1, "items._id": 1 } ).pretty()
