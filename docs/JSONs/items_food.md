# Food Items
Documents that fall into the drinks / beverages category of items

__There is a total of 8 items here__
___

**Char Siew Bao**
```json
{
    "name": "Char Siew Bao",
    "altName": [
        "Char Siu Bao",
        "Steamed BBQ Pork Buns",
        "BBQ Pork Buns",
        "Barbecue Pork Buns",
        "Pork Buns",
        "Bao Buns",
        "叉烧包",
        "叉燒包",
        "chāshāo bāo",
        "chāsīu bāau"
    ],
    "description": "Cantonese Style, Steamed Barbecue Pork Buns.",
    "tags": [
        "Food",
        "Dim Sum",
        "Yum Cha",
        "Steamed",
        "Pork",
        "Bun",
        "Buns",
        "Bao",
        "Bao Buns"
    ],
    "image": {
        "thumbnail": "http://bambamboo.com.au/dish/101004.jpg",
        "full": "http://bambamboo.com.au/dish/101004.jpg"
    },
    "price": {
        "lunch": 5.00,
        "dinner": 5.50,
        "holiday": 6.50,
        "special": 2.50
    },
    "isOnSale": false,
    "qty": 500
}
```

**Siew Mai**
```json
{
    "name": "Siew Mai",
    "altName": [
        "Prawn and Pork Dumplings",
        "Shu Mai",
        "Shumai",
        "Shui Mai",
        "Sui Mai",
        "Shui Mei",
        "Siu Mai",
        "Shao Mai",
        "xíu mại",
        "烧卖",
        "燒賣",
        "shāomài",
        "sīumáai"
    ],
    "description": "Cantonese Style, Pork and Prawn Dumplings.",
    "tags": [
        "Food",
        "Dim Sum",
        "Yum Cha",
        "Steamed",
        "Dumpling",
        "Dumplings",
        "Pork",
        "Prawn"
    ],
    "image": {
        "thumbnail": "http://bambamboo.com.au/dish/101003.jpg",
        "full": "http://bambamboo.com.au/dish/101003.jpg"
    },
    "price": {
        "lunch": 5.50,
        "dinner": 6.00,
        "holiday": 6.50,
        "special": 3.00
    },
    "isOnSale": false,
    "qty": 500
}
```

**Xiao Long Bao**
```json
{
    "name": "Xiao Long Bao",
    "altName": [
        "Xiaolongbao",
        "Xiaolong Bao",
        "Xiaolong Mantou",
        "Soup Dumplings",
        "Soup Bun",
        "Shanghai Style Soup Dumplings",
        "小笼包",
        "小籠包",
        "小笼馒头",
        "小籠饅頭"
    ],
    "description": "Shanghai Style, Juicy Steamed Pork Soup Dumplings.",
    "tags": [
        "Food",
        "Dim Sum",
        "Yum Cha",
        "Steamed",
        "Dumpling",
        "Dumplings",
        "Soup",
        "Soup Dumpling",
        "Soup Dumplings",
        "Pork"
    ],
    "image": {
        "thumbnail": "http://bambamboo.com.au/dish/101006.jpg",
        "full": "http://bambamboo.com.au/dish/101006.jpg"
    },
    "price": {
        "lunch": 5.50,
        "dinner": 6.00,
        "holiday": 6.50,
        "special": 3.00
    },
    "isOnSale": false,
    "qty": 250
}
```

**Fried Rice**
```json
{
    "name": "Chicken Fried Rice",
    "altName": [
        "叉烧鸡炒饭"
    ],
    "description": "Western Style, Chicken Fried Rice",
    "tags": [
        "Food",
        "Rice",
        "Fried Rice"
    ],
    "image": {
        "thumbnail": "http://bambamboo.com.au/dish/104007.jpg",
        "full": "http://bambamboo.com.au/dish/104007.jpg"
    },
    "price": {
        "lunch": 4.50,
        "dinner": 5.00,
        "holiday": 5.50,
        "special": 3.50
    },
    "qty": 500
}
```

**Steamed Rice**
```json
{
    "name": "Steamed Rice",
    "altName": [
        "Rice",
        "Plain Rice",
        "泰国香米饭"
    ],
    "description": "Steamed Jasmine Rice.",
    "tags": [
        "Food",
        "Rice",
        "Steamed"
    ],
    "image": {
        "thumbnail": "http://bambamboo.com.au/dish/104009.jpg",
        "full": "http://bambamboo.com.au/dish/104009.jpg"
    },
    "price": {
        "lunch": 2.50,
        "dinner": 3.00,
        "holiday": 3.00,
        "special": 2.00
    },
    "qty": 800
}
```
**Honey BBQ Chicken with Greens**
```json
{
    "name": "Honey BBQ Chicken with Greens",
    "altName": [
        "蜜汁烧鸡配青菜",
        "BBQ Chicken",
        "Barbecue Chicken"
    ],
    "description": "Western Chinese Fusion, Honey Barbecue Chicken with side of Greens.",
    "tags": [
        "Food",
        "Barbecue",
        "BBQ",
        "Chicken",
        "Greens",
        "Honey"
    ],
    "image": {
        "thumbnail": "http://bambamboo.com.au/dish/1020071.jpg",
        "full": "http://bambamboo.com.au/dish/1020071.jpg"
    },
    "price": {
        "lunch": 10.50,
        "dinner": 12.50,
        "holiday": 13.50,
        "special": 8.50
    },
    "qty": 48
}
```

**Honey BBQ Pork with Greens**
```json
{
    "name": "Honey BBQ Pork with Greens",
    "altName": [
        "Char Siew",
        "Chasu",
        "Char Siu",
        "Chashao",
        "Cha Sio",
        "叉燒",
        "chāsīu",
        "BBQ Pork",
        "Barbecue Pork",
        "叉烧配青菜"
    ],
    "description": "Guangdong Style, Honey Barbecue Pork with side of Greens.",
    "tags": [
        "Food",
        "Barbecue",
        "BBQ",
        "Pork",
        "Greens",
        "Honey"
    ],
    "image": {
        "thumbnail": "http://bambamboo.com.au/dish/1020112.jpg",
        "full": "https://upload.wikimedia.org/wikipedia/commons/9/95/Charsiu.jpg"
    },
    "price": {
        "lunch": 10.50,
        "dinner": 12.50,
        "holiday": 13.50,
        "special": 8.50
    },
    "qty": 48
}
```

**Zhajiang Noodles**
```json
{
    "name": "Zhajiang Noodles",
    "altName": [
        "Zhajiangmian",
        "炸醬面",
        "Beijing Signature Noodles",
        "Zha Jiang Main",
        "Soybean Paste Noodles",
        "Fried Sauce Noodles"
    ],
    "description": "Beijing Style, Simmering Stir-Fried Soybean Paste Noodles",
    "tags": [
        "Food",
        "Noodles",
        "Soybean",
        "Pork",
        "Wheat Noodle",
        "Wheat Noodles",
        "Stir Fry",
        "Soybean Paste",
        "Beijing"
    ],
    "image": {
        "thumbnail": "http://bambamboo.com.au/dish/103001.jpg",
        "full": "https://i0.wp.com/redhousespice.com/wp-content/uploads/2017/05/Zha-Jiang-Mian-landscape4.jpg?resize=768%2C432&ssl=1"
    },
    "price": {
        "lunch": 10.50,
        "dinner": 12.50,
        "holiday": 14.50,
        "special": 9.00
    },
    "qty": 24
}
```
