## Testing Documents
Documents used for testing functionality of the APIs that are non-critical

___

**DELETEME**
```json
{
    "name": "DELETEME",
    "description": "Test document used for API deletion functionality testing",
    "tags": [
        "Test",
        "Testing",
        "Dev",
        "Development",
        "DELETE",
        "DELETE ME"
    ],
    "price": {
        "lunch": 1.00,
        "dinner": 2.00,
        "holiday": 3.00
    },
    "qty": 1
}
```

**EDITME**
```json
{
    "name": "EDITME",
    "description": "Test document used for API edit functionality testing",
    "tags": [
        "Test",
        "Testing",
        "Dev",
        "Development",
        "EDIT",
        "EDIT ME"
    ],
    "price": {
        "lunch": 1.00,
        "dinner": 2.00,
        "holiday": 3.00
    },
    "qty": 1,
    "isAvailable": {
        "lunch": false,
        "dinner": false,
        "holiday": false
    }
}
```
