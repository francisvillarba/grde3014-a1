## Drinks
Documents that fall into the drinks / beverages category of items

__There is a total of 9 items here__
___

**Water**
```json
{
    "name": "Water",
    "altName": [
        "Still Water",
        "Spring Water",
        "Bottle Water",
        "Bottled Water",
        "Mount Franklin",
        "Mt. Franklin",
        "Plain Water",
        "Filtered Water"
    ],
    "description": "Australian spring water of exceptional quality and purity, sourced from specially selected springs around the country.",
    "tags": [
        "Beverage",
        "Drink",
        "Drinks",
        "Water"
    ],
    "image": {
        "thumbnail": "https://www.coca-colacompany.com/content/dam/journey/au/en/brand-detail/mt-franklin/Large%20product%20shot%20-%20Mt%20Franklin%20Spring%20Still%20Water.png",
        "full": "https://www.coca-colacompany.com/content/dam/journey/au/en/brand-detail/mt-franklin/Large%20product%20shot%20-%20Mt%20Franklin%20Spring%20Still%20Water.png"
    },
    "price": {
        "lunch": 3.30,
        "dinner": 3.50,
        "holiday": 3.50,
        "sale": 3.00
    },
    "qty": 80
}
```
**Sparkling Water**
```json
{
   "name": "Sparkling Water",
   "altName": [
        "Carbonated Water",
        "Soda Water",
        "Fizzy Water",
        "Bubbly Water"
   ],
   "description": "Australian spring water infused with bubbles. The perfect choice to add a little sparkle to your day.",
   "tags": [
        "Beverage",
        "Drink",
        "Drinks",
        "Sparkling",
        "Soda",
        "Carbonated"
   ],
   "image": {
       "thumbnail": "https://www.coca-colacompany.com/content/dam/journey/au/en/brand-detail/mt-franklin/Large%20product%20shot%20-%20Mt%20Franklin%20Lightly%20Sparkling%20Water.png",
       "full": "https://www.coca-colacompany.com/content/dam/journey/au/en/brand-detail/mt-franklin/Large%20product%20shot%20-%20Mt%20Franklin%20Lightly%20Sparkling%20Water.png"
   },
   "price": {
       "lunch": 3.50,
       "dinner": 3.70,
       "holiday": 3.70,
       "sale": 3.30
   },
   "qty": 96
}
```

**Coca Cola**
```json
{
    "name": "Coca Cola",
    "altName": [
        "Coke",
        "Coke Can",
        "Coca Cola Classic",
        "Cola",
        "Coke Classic"
    ],
    "description": "Taste the feeling. Nothing beats the taste of Coca Cola Classic",
    "tags": [
        "Beverage",
        "Drink",
        "Drinks",
        "Coke",
        "Cola",
        "Caffeine",
        "Carbonated"
    ],
    "image": {
        "thumbnail": "https://www.coca-colacompany.com/content/dam/journey/au/en/brand-detail/coca-cola/Large%20product%20shot%20-%20Coca-Cola%20Classic.png",
        "full": "https://www.coca-colacompany.com/content/dam/journey/au/en/brand-detail/coca-cola/Large%20product%20shot%20-%20Coca-Cola%20Classic.png"
    },
    "price": {
        "lunch": 2.00,
        "dinner": 2.00,
        "holiday": 2.00,
        "sale": 2.00
    },
    "qty": 120
}
```

**Coca Cola No Sugar**
```json
{
   "name": "Coca Cola No Sugar",
   "altName": [
        "Coke No Sugar",
        "Diet Coke",
        "Diet Cola",
        "Sugar Free Coke",
        "Coke No Sugar Can"
   ],
   "description": "The taste you love, with no sugar.",
   "tags": [
        "Beverage",
        "Drink",
        "Drinks",
        "Coke",
        "Cola",
        "Caffeine",
        "Carbonated",
        "No Sugar",
        "Sugar Free",
        "Diet"
   ],
   "image": {
       "thumbnail": "https://www.coca-colacompany.com/content/dam/journey/au/en/brand-detail/coca-cola/Large%20product%20shot%20-%20Coke%20No%20Sugar.png",
       "full": "https://www.coca-colacompany.com/content/dam/journey/au/en/brand-detail/coca-cola/Large%20product%20shot%20-%20Coke%20No%20Sugar.png"
   },
   "price": {
       "lunch": 2.00,
       "dinner": 2.00,
       "holiday": 2.00,
   },
   "qty": 120
}
```

**Sprite**
```json
{
    "name": "Sprite",
    "altName": [
        "Sprite Lemon-Lime",
        "Sprite Lemon Lime",
        "Sprite Bottle",
        "Lemonade"
    ],
    "description": "A unique lemon-lime hit that is perfect for moments of refreshment.",
    "tags": [
        "Beverage",
        "Drink",
        "Drinks",
        "Lemonade",
        "Carbonated"
    ],
    "image": {
        "thumbnail": "https://www.coca-colacompany.com/content/dam/journey/au/en/brand-detail/sprite/Large%20product%20Shot_Sprite_LemonLime_600mL.png",
        "full": "https://www.coca-colacompany.com/content/dam/journey/au/en/brand-detail/sprite/Large%20product%20Shot_Sprite_LemonLime_600mL.png"
    },
    "price": {
        "lunch": 4.00,
        "dinner": 4.50,
        "holiday": 4.50,
        "sale": 3.50
    },
    "qty": 96
}
```

**Fanta**
```json
{
    "name": "Fanta",
    "altName": [
        "Fanta Orange",
        "Orange Soda"
    ],
    "description": "Bright and Bubbly, Tangy and Fruity, a drink that intensifies fun.",
    "tags": [
        "Beverage",
        "Drink",
        "Drinks",
        "Orange",
        "Carbonated"
    ],
    "image": {
        "thumbnail": "https://www.coca-colacompany.com/content/dam/journey/au/en/brand-detail/fanta/Large%20product%20shot%20-%20Fanta%20Orange.png",
        "full": "https://www.coca-colacompany.com/content/dam/journey/au/en/brand-detail/fanta/Large%20product%20shot%20-%20Fanta%20Orange.png"
    },
    "price": {
        "lunch": 4.00,
        "dinner": 4.50,
        "holiday": 4.50,
        "sale": 3.50
    },
    "qty": 72
}
```

**Apple Juice**
```json
{
    "name": "Apple Juice",
    "altName": [
        "Keri Apple",
        "Keri Apple Juice"
    ],
    "description": "Australian made Apple Juice",
    "tags": [
        "Beverage",
        "Drink",
        "Drinks",
        "Apple",
        "Juice"
    ],
    "image": {
        "thumbnail": "https://www.coca-colacompany.com/content/dam/journey/au/en/brand-detail/keri-juice/Large%20Product%20Shot%20-%20Keri%20Apple.png",
        "full": "https://www.coca-colacompany.com/content/dam/journey/au/en/brand-detail/keri-juice/Large%20Product%20Shot%20-%20Keri%20Apple.png"
    },
    "price": {
        "lunch": 3.50,
        "dinner": 3.80,
        "holiday": 3.80,
        "sale": 3.20
    },
    "qty": 36
}
```

**Orange Juice**
```json
{
    "name": "Orange Juice",
    "altName": [
        "Keri Orange",
        "Keri Orange Juice"
    ],
    "description": "Australian made Orange Juice",
    "tags": [
        "Beverage",
        "Drink",
        "Drinks",
        "Orange",
        "Juice"
    ],
    "image": {
        "thumbnail": "https://www.coca-colacompany.com/content/dam/journey/au/en/brand-detail/keri-juice/Large%20Product%20Shot%20-%20Keri%20Orange.png",
        "full": "https://www.coca-colacompany.com/content/dam/journey/au/en/brand-detail/keri-juice/Large%20Product%20Shot%20-%20Keri%20Orange.png"
    },
    "price": {
        "lunch": 3.50,
        "dinner": 3.80,
        "holiday": 3.80,
        "sale": 3.20
    },
    "qty": 48
}
```

**Appletiser**
```json
{
    "name": "Appletiser",
    "altName": [
        "Sparkling Apple",
        "Sparkling Apple Juice"
    ],
    "description": "Non-alcoholic sparkling apple juice, expertly blended to elevate the moment.",
    "tags": [
        "Beverage",
        "Drink",
        "Drinks",
        "Apple",
        "Sparkling",
        "Apple Juice",
        "Non-Alcoholic",
        "Carbonated"
    ],
    "image": {
        "thumbnail": "https://www.coca-colacompany.com/content/dam/journey/au/en/brand-detail/appletiser/Large%20Product%20Shot%20-%20Appletiser%20Sparkling%20Apple%20Juice.png",
        "full": "https://www.coca-colacompany.com/content/dam/journey/au/en/brand-detail/appletiser/Large%20Product%20Shot%20-%20Appletiser%20Sparkling%20Apple%20Juice.png"
    },
    "price": {
        "lunch": 4.00,
        "dinner": 4.50,
        "holiday": 4.50,
        "sale": 3.50
    },
    "qty": 48
}
```
