# Orders
List of JSONs used to create the user documents in the users collection

__There is a total of {Undefined} items here__

___

## Completed Orders

**Test Order**

This is a test order that does not correspond to an actual user or item

```json
{
	"user": "5e845fcdb8f0fe097742369b",
	"target": 1,
	"status": 0,
	"items": [
		{
			"id": "5e7afee7d393e52dcdcd8f0b",
			"qty": 1,
			"listPrice": 2.50,
			"finalPrice": 2.50
		},
		{
			"id": "5e7afee7d393e52dcdcd8f0b",
			"qty": 1,
			"listPrice": 2.50,
			"finalPrice": 2.50,
			"notes": [
				{
					"flag": 3,
					"comment": "Example Order Note"
				},
                {
                    "flag": 3,
                    "comment": "Items and user will not correspond to actual documents"
                }
			]
		}
	],
	"payment": {
		"totalPrice": 5.00
	}
}
```

**Orders that Francis_NOT ADMIN (ME!) owns**

Example with two items completely free

```json
{
    "user": "5e84947380748e457dbf41f5",
    "target": 1,
    "status": 4,
    "isNotified": true,
    "isCompleted": true,
    "items": [
        {
            "id": "5e803991d52b073a1f687029",
            "qty": 1,
            "listPrice": 3.50,
            "finalPrice": 0.00,
            "notes": [
                {
                    "flag": 1,
                    "comment": "Free for workers"
                }
            ]
        },
        {
            "id": "5e81a3c76ec58c0f91aed015",
            "qty": 2,
            "listPrice": 6.00,
            "finalPrice": 12.00
        },
        {
            "id": "5e81a3d46ec58c0f91aed017",
            "qty": 1,
            "listPrice": 3.00,
            "finalPrice": 0.00,
            "notes": [
                {
                    "flag": 1,
                    "comment": "Free for workers"
                }
            ]
        }
    ],
    "payment": {
        "method": 3,
        "totalPrice": 12.00,
        "processor": "5e84945080748e457dbf41f0"
    },
    "isPaid": true
}
```

Example with a mix of discount, free and full price and differing notes.

```json
{
    "user": "5e84947380748e457dbf41f5",
    "target": 1,
    "status": 4,
    "isNotified": true,
    "isCompleted": true,
    "items": [
        {
            "id": "5e81a3166ec58c0f91aed012",
            "qty": 1,
            "listPrice": 4.50,
            "finalPrice": 2.25,
            "notes": [
                {
                    "flag": 1,
                    "comment": "Staff discount 50% off"
                }
            ]
        },
        {
            "id": "5e81a3ac6ec58c0f91aed013",
            "qty": 1,
            "listPrice": 5.50,
            "finalPrice": 5.50
        },
        {
            "id": "5e81a3ce6ec58c0f91aed016",
            "qty": 1,
            "listPrice": 5.00,
            "finalPrice": 0.00,
            "notes": [
                {
                    "flag": 1,
                    "comment": "Free for workers"
                }
            ]
        }
    ],
    "payment": {
        "method": 3,
        "totalPrice": 7.75,
        "processor": "5e84945080748e457dbf41f0"
    },
    "isPaid": true
}
```

**Orders by Andrew**

Example where customer only popped in to purchase a drink

```json
{
    "user": "5e84947d80748e457dbf41f6",
    "target": 2,
    "status": 4,
    "isNotified": false,
    "isCompleted": true,
    "items": [
        {
            "id": "5e803b86d52b073a1f68702a",
            "qty": 1,
            "listPrice": 3.50,
            "finalPrice": 3.50
        }
    ],
    "payment": {
        "method": 1,
        "totalPrice": 3.50,
        "processor": "5e84945080748e457dbf41f0"
    },
    "isPaid": true
}
```

**Delivery Order**

In this order, we have two ingredient changes. You will also notice that
the processor is the same as the user. This is because delivery partners
are also the one that pays for the order too.

```json
{
    "user": "5e857bcfe751b912e66afc63",
    "target": 3,
    "status": 4,
    "isNotified": false,
    "isCompleted": true,
    "items": [
        {
            "id": "5e81a3db6ec58c0f91aed018",
            "qty": 1,
            "listPrice": 10.50,
            "finalPrice": 10.50
        },
        {
            "id": "5e803e12d52b073a1f68702b",
            "qty": 2,
            "listPrice": 2.00,
            "finalPrice": 4.00
        },
        {
            "id": "5e81a3f56ec58c0f91aed01a",
            "qty": 1,
            "listPrice": 10.50,
            "finalPrice": 10.50,
            "notes": [
                {
                    "flag": 6,
                    "comment": "No Coriander"
                },
                {
                    "flag": 6,
                    "comment": "More Chilli"
                }
            ]
        }
    ],
    "payment": {
        "method": 5,
        "totalPrice": 25.00,
        "processor": "5e857bcfe751b912e66afc63"
    },
    "isPaid": true
}
```

**In progress Orders**

Example of a walk in customer that is waiting for order to complete

```json
{
    "user": "5e857bede751b912e66afc64",
    "target": 1,
    "status": 1,
    "isNotified": false,
    "isCompleted": false,
    "items": [
        {
            "id": "5e81a3e26ec58c0f91aed019",
            "qty": 1,
            "listPrice": 10.50,
            "finalPrice": 10.50
        },
        {
            "id": "5e81a3d46ec58c0f91aed017",
            "qty": 1,
            "listPrice": 2.50,
            "finalPrice": 2.50
        },
        {
            "id": "5e81a3166ec58c0f91aed012",
            "qty": 1,
            "listPrice": 4.00,
            "finalPrice": 4.00
        },
        {
            "id": "5e803991d52b073a1f687029",
            "qty": 1,
            "listPrice": 3.30,
            "finalPrice": 3.30
        }
    ],
    "payment": {
        "method": 2,
        "totalPrice": 20.30,
        "processor": "5e84945080748e457dbf41f0"
    },
    "isPaid": true
}
```

Example of an order that is pending completion

```json
{
    "user": "5e857bede751b912e66afc64",
    "target": 1,
    "status": 0,
    "isNotified": false,
    "isCompleted": false,
    "items": [
        {
            "id": "5e81a3e26ec58c0f91aed019",
            "qty": 1,
            "listPrice": 10.50,
            "finalPrice": 10.50
        }
    ],
    "isPaid": false
}
```
