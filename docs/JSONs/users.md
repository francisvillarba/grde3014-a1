# User Accounts
List of JSONs used to create the user documents in the users collection

__There is a total of {Undefined} items here__

___

## Template Accounts

Example accounts to be used with the system

___

**Admin**

The main admin account for the database. Think of it like the database owner.
```json
{
    "username": "admin",
    "password": "admin123",
    "email": "admin@example.com.au",
    "name": "Admin Account",
    "privileges": "Owner"
}
```

**Manager**

An example account for managers. The idea is that managers would not have
access to all the functionality as the owner would but have way more access
then Point of Sale, Kitchen and Users.

```json
{
    "username": "manager",
    "password": "manager123",
    "email": "manager@example.com.au",
    "name": "Manager Account",
    "privileges": "Manager"
}
```
**Kitchen**

An example account for the kitchen staff, only has view only access to current
orders that is in the "Processing" stage.

```json
{
    "username": "kitchen",
    "password": "kitchen123",
    "email": "kitchen@example.com.au",
    "name": "Kitchen Account",
    "privileges": "Kitchen"
}
```

**Point of Sale**

An example account for point of sales staff. Has limited access to the system,
outside of creating new orders, pos reconciliation etc.

```json
{
    "username": "pos",
    "password": "pos123",
    "email": "pos@example.com.au",
    "name": "Point of Sales Account",
    "privileges": "POS"
}
```
**Website Account**

The account for the website, where it has limited, read-only access to certain
collections and documents on the database.

```json
{
    "username": "web",
    "password": "webadmin123",
    "email": "webmaster@example.com.au",
    "name": "Website Account",
    "privileges": "Banned"
}
```

**DELETEME**

For testing the deletion functionality of the system

```json
{
    "username": "DELETEME",
    "password": "DELETEME123",
    "email": "deleteme@example.com.au",
    "name": "DELETION TEST ACCOUNT",
    "privileges": "Banned"
}
```

**EDITME**

For testing the editing functionality of the system

```json
{
    "username": "EDITME",
    "password": "EDITME123",
    "email": "editme@example.com.au",
    "name": "EDIT TEST ACCOUNT",
    "privileges": "User"
}
```

Changing editme's password to editedme123
```json
{
    "password": "editedme123"
}
```

**Delivery Partner**

A template account for delivery partners to the restaurant. i.e. Uber Eats,
Menu Log, Deliveroo etc...

```json
{
    "username": "delivery",
    "password": "delivery123",
    "email": "delivery@example.com.au",
    "name": "Delivery Partner Account",
    "privileges": "User"
}
```

**No Account**

Template account for non-registered users, i.e. walk in customer

```json
{
    "username": "None",
    "password": "NotRegisteredGuest123",
    "email": "guest@example.com.au",
    "name": "Guest Account",
    "privileges": "Banned"
}

___

## User Accounts

Some user accounts that I have created, to have some more examples

___

**Francis (MY ACCOUNT!)**

My personal account for the system. Generally speaking it is a good idea to
separate your accounts from admin and main to reduce the chance of compromise
due to social engineering / over the shoulder techniques ;)

```json
{
    "username": "francisAdmin",
    "password": "francisAdmin123",
    "email": "francis.villarba@student.curtin.edu.au",
    "isEmailValidated": true,
    "name": "Francis Villarba ADMIN",
    "marketing": false,
    "privileges": "Owner"
}
```

**Francis (MY ACCOUNT!)**

My personal account for the system. This time without all-reaching access to
the entire system xD

```json
{
    "username": "francis",
    "password": "francis123",
    "email": "dragonfable133-webmaster@yahoo.com.au",
    "isEmailValidated": true,
    "name": "Francis Villarba",
    "marketing": true,
    "privileges": "User"
}
```

**Andrew**

An account for a user named andrew who decides to create a new order

```json
{
    "username": "andrew",
    "password": "orderplease",
    "email": "andrew.orders@example.com.au",
    "isEmailValidated": true,
    "name": "Andrew Orders",
    "marketing": true,
    "privileges": "User"
}
```
