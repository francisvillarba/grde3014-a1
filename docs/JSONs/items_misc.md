# Misc Items
Documents that fall into the misc category of items

__There is a total of {Undefined} items here__

___

**Small Keep Cup**
```json
{
    "name": "Small Keep Cup",
    "altName": [
        "Small Glass Cup",
        "Small Tea Cup",
        "Small Reusable Cup"
    ],
    "description": "Small, Reusable Glass Cup for Tea. 250ml",
    "tags": [
        "Merchandise",
        "Miscellaneous",
        "Cup",
        "Glass",
        "Tea",
        "Tea Cup",
        "Reusable",
        "Keep Cup"
    ],
    "image": {
        "thumbnail": "https://myer-media.com.au/wcsstore/MyerCatalogAssetStore/images/50/503/4231/500/9/614910430/614910430_5_1_720x928.jpg",
        "full": "https://myer-media.com.au/wcsstore/MyerCatalogAssetStore/images/50/503/4231/500/9/614910430/614910430_5_1_720x928.jpg"
    },
    "price": {
        "lunch": 24.95,
        "dinner": 24.95,
        "holiday": 24.95,
        "special": 17.50
    },
    "qty": 96
}
```

**Medium Keep Cup**
```json
{
    "name": "Medium Keep Cup",
    "altName": [
        "Medium Glass Cup",
        "Medium Tea Cup",
        "Medium Reusable Cup"
    ],
    "description": "Medium, Reusable Glass Cup for Tea. 450ml",
    "tags": [
        "Merchandise",
        "Miscellaneous",
        "Cup",
        "Glass",
        "Tea",
        "Tea Cup",
        "Reusable",
        "Keep Cup"
    ],
    "image": {
        "thumbnail": "https://myer-media.com.au/wcsstore/MyerCatalogAssetStore/images/50/503/4231/500/9/614910520/614910520_5_1_720x928.jpg",
        "full": "https://myer-media.com.au/wcsstore/MyerCatalogAssetStore/images/50/503/4231/500/9/614910520/614910520_5_1_720x928.jpg"
    },
    "price": {
        "lunch": 27.95,
        "dinner": 27.95,
        "holiday": 27.95,
        "special": 20.50
    },
    "qty": 128
}
```

**Glass Teapot**
```json
{
    "name": "Glass Teapot",
    "altName": [
        "Teapot"
    ],
    "description": "Large, Glass Teapot, Great for enjoying our tea at home! 850ml",
    "tags": [
        "Merchandise",
        "Miscellaneous",
        "Glass",
        "Tea",
        "Teapot"
    ],
    "image": {
        "thumbnail": "https://www.t2tea.com/dw/image/v2/AASF_PRD/on/demandware.static/-/Sites-masterCatalog_t2/default/dw29644ff9/images/products/H205BA168_t2-teaset-tall-glass-black-teapot_p1.png?sw=262&sh=262&sm=fit",
        "full": "https://www.t2tea.com/dw/image/v2/AASF_PRD/on/demandware.static/-/Sites-masterCatalog_t2/default/dw29644ff9/images/products/H205BA168_t2-teaset-tall-glass-black-teapot_p1.png?sw=555&sh=555&sm=fit"
    },
    "price": {
        "lunch": 24.95,
        "dinner": 24.95,
        "holiday": 24.95,
        "special": 19.50
    },
    "qty": 72
}
