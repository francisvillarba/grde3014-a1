'use strict';

/**
 * order
 *
 * The order model for mongoose and mongodb. For use with GRDE3014, Web
 * Authoring Design, Assignment 1.
 *
 * @author  Francis Villarba <francis.villarba@student.curtin.edu.au>
 * @version 1.1.5
 *
 * @requires mongoose
 */

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema for notes documents (to make it easier to do nested documents)
const OrderNotesSchema = new Schema({
    flag: {
        // What type of note is this?
        // StaffDiscount, Discount, Promotion, Refund, Exchange, IngredientChange, 7 - 9 Future use
        type: Number,
        enum: [1, 2, 3, 4, 5, 6, 7, 8, 9]
    },
    comment: {
        // The main body of the note
        type: String
    }
});

// Schema for order items listing documents
const OrderItemsSchema = new Schema({
    id: {
        // The ObjectId of the item document from the Items collection
        type: Schema.Types.ObjectId,
        required: true
    },
    qty: {
        // The quantity of the item to order
        type: Number,
        required: false,
        default: 1
    },
    listPrice: {
        // The original price of the item to maintain history consistency, should the price change in the future
        type: Schema.Types.Decimal128,
        required: true
    },
    finalPrice: {
        // The final price of this item, typically same as listPrice but can change due to discount etc.
        type: Schema.Types.Decimal128,
        required: true
    },
    notes: [OrderNotesSchema]
    // Any notes attached to this particular item such as 'no coriander', 'staff discount' etc.
});

// Schema for payment information document
const PaymentInfoSchema = new Schema({
    method: {
        // What did they pay with?
        // NotPaid, Cash, Card, AMEX, AliPay, 5-9 Future Use
        type: Number,
        enum: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
        default: 0
    },
    totalPrice: {
        // The total price of the order
        type: Schema.Types.Decimal128,
        default: 0.00
    },
    processedAt: {
        // When did the payment occur?
        type: Date,
        default: null
    },
    processor: {
        // Who processed the payment? Good for final day checking of POS staff
        // Use the ObjectId from the users collection
        type: Schema.Types.ObjectId
    }
});

// The main order schema
const OrderSchema = new Schema({
    user: {
        // Who owns this order / who requested this order?
        type: Schema.Types.ObjectId,    // User's ObjectId from Users Collection
        required: true
    },
    target: {
        // What type of order is it?
        // DineIn, TakeAway, Deliver, ScheduledDineIn, ScheduledTakeAway, ScheduledDeliver, 7-9 Future Use
        type: Number,
        required: true,
        enum: [1, 2, 3, 4, 5, 6, 7, 8, 9],
        index: true
    },
    scheduledFor: {
        // If it is a scheduled order, what is the target collection time?
        // If not supplied, default to current date and time + 10 mins (average)
        type: Date,
        required: false,
        default: Date.now() + 600000
    },
    status: {
        // What is the status of the order?
        // PaymentRequired, InProgress, ReadyToCollect, Completed, Problem, 5-9 Future use
        // Index status so we can make compound indexes for efficient searching - Future use
        type: Number,
        required: false,
        enum: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
        default: 0,
        index: true
    },
    isNotified: {
        // Have we notified the customer that the order is ready for collection yet?
        type: Boolean,
        required: false,
        default: false
    },
    isCompleted: {
        // Has the order been completed yet?
        type: Boolean,
        required: false,
        default: false
    },
    items: [OrderItemsSchema],
    payment: PaymentInfoSchema,
    isPaid: {
        // Has the order been paid yet?
        type: Boolean,
        required: false,
        default: false
    }
},
    {
        // Timestamps so we can keep track of creation and modified info
        timestamps: true,
        versionKey: false
    }
);

module.exports = mongoose.model('Order', OrderSchema);
