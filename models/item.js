'use strict';

/**
 * item
 *
 * The item model for mongoose and mongodb. For use with GRDE3014, Web
 * Authoring Design, Assignment 1.
 *
 * @author  Francis Villarba <francis.villarba@student.curtin.edu.au>
 * @version 1.0.0
 *
 * @requires mongodb
 * @requires mongoose
 */

// Setup  ------------------------------------------------------------------- //
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema  ------------------------------------------------------------------ //
const ItemSchema = new Schema({
    name: {
        // What is the name of the item
        type: String,
        required: true,
        index: true,        // Index to make searching quicker
        unique: true
        // No point having a duplicate of the same item
    },
    altName: [
        {
            // Some items have alt names i.e. sparkling water == soda water.
            type: String,
            required: false,
            unique: false,
            // Would prefer to have unique, but undefined can have issues
            // Perhaps we can try using partialFilterExpressions? More research required
            default: null
            // null if there is no alternative names for the dish
        }
    ],
    description: {
        // Describe the item
        type: String,
        required: true
    },
    tags: [
        // To enable categorisation, search and sorting, i.e. drinks, food
        // Enable indexing on tags to allow for Compound Indexes - Future Use
        {
            type: String,
            required: false,
            index: true,
            default: 'TO BE TAGGED'
        // Make it clear that this still needs tags in the future
        }
    ],
    image: {
        thumbnail: {
            // For viewing the picture on a grid
            type: String,
            required: false,
            default: 'https://upload.wikimedia.org/wikipedia/commons/d/d2/Question_mark.svg'
        },
        full: {
            // For detail viewing
            type: String,
            required: false,
            default: 'https://upload.wikimedia.org/wikipedia/commons/d/d2/Question_mark.svg'
        }
    },
    price: {
        lunch: {
            // Lunch Time Pricing
            type: Schema.Types.Decimal128,
            required: true
        },
        dinner: {
            // Dinner Pricing
            type: Schema.Types.Decimal128,
            required: true
        },
        holiday: {
            // Public Holiday Pricing
            type: Schema.Types.Decimal128,
            required: true
        },
        sale: {
            // Sale Pricing
            type: Schema.Types.Decimal128,
            required: false,
            default: 0.00
        }
    },
    isOnSale: {
        // Is the item on special right now?
        type: Boolean,
        required: false,
        default: false
    },
    isAvailable: {
        lunch: {
            // Is it available at lunch time?
            type: Boolean,
            required: false,
            default: true
        },
        dinner: {
            // Is it available at dinner time?
            type: Boolean,
            required: false,
            default: true
        },
        holiday: {
            // Is it available on public holidays?
            type: Boolean,
            required: false,
            default: true
        }
    },
    qty: {
        // The quantity of this item?
        type: Number,
        required: false,
        default: 0
    }
},
    {
        // Timestamps so we can keep track of creation and modified info
        timestamps: true,
        versionKey: false
    }
);

module.exports = mongoose.model('Item', ItemSchema);
