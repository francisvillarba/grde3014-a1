'use strict';

/**
 * user.js
 *
 * The user model for mongoose and mongodb. For use with GRDE3014, Web
 * Authoring Design, Assignment 1.
 *
 * Encryption of passwords is inspired by mongoDB Documentation
 * https://www.mongodb.com/blog/post/password-authentication-with-mongoose-part-1
 *
 * @author  Francis Villarba <francis.villarba@student.curtin.edu.au>
 * @version 2.1.1
 *
 * @requires mongoose
 * @requires bcrypt
 */

// Setup  ------------------------------------------------------------------- //
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Using bcrypt encryption for safe password storage!
const bcrypt = require('bcrypt');                           // For Encryption
const SALT_WORK_FACTOR = 10;                                // Encryption Factor

// Schema ------------------------------------------------------------------- //
const UserSchema = new Schema({
    username: {
        type: String,
        required: true,
        index: true,                    // Index to allow for compound indexes
        unique: true                    // Unique usernames for login
    },
    password: {
        type: String,
        required: true
        // Store Hashed Passwords Only! Use mongoose bcrypt middleware
    },
    email: {
        type: String,
        required: true,
        unique: true                    // To prevent duplicate accounts
    },
    isEmailValidated: {
        type: Boolean,
        required: false,
        default: false
    },
    name: {
        type: String,
        required: true
    },
    mobile: {
        type: String,
        required: false
    },
    marketing: {
        // Did they agree to recieve offers and communication emails?
        type: Boolean,
        required: false,
        default: false                     // Default to false for GDPR
    },
    lastLogin: {
        type: Date,
        default: Date.now
    },
    privileges: [{
        type: String,
        enum: ['Banned', 'User', 'POS', 'Kitchen', 'Manager', 'Owner']
        // Use enum to force only these values to be used
    }],
    orderHistory: [{
        // Order history is an array of Order ObjectIds
        type: Schema.Types.ObjectId,
        required: false
        }
    ]
},
    {
        // Timestamps so we can keep track of creation and modified info
        timestamps: true,
        versionKey: false
    }
);

// Middleware Functionality ------------------------------------------------- //

/**
 * What to do before we save, edit or modify the user database
 *
 * @module user
 * @function
 * @param {Object} save - The save function / command (via mongoose)
 * @return {Object} next - The callback / command to execute (via mongoose)
 */

UserSchema.pre( 'save', function(next) {
    var cUser = this;                                    // cUser == current user

    // Only hash / rehash the password if the password field has been modified or if it is newly created
    if( !cUser.isModified('password') ) {
        // If we didn't modify the password field, skip
        return next();
    } else {
        // Generate the salt
        bcrypt.genSalt( SALT_WORK_FACTOR, function(err, salt) {
            if(err) {
                // If an error has occured when generating
                return next(err);   // Add to the callback, the error
            } else {
                // If all is good, run the hashing function
                bcrypt.hash( cUser.password, salt, function(err, hash) {
                    if(err) {
                        // If an error has occured when generating the hashed pw
                        return next(err);       // Add err to callstack
                    } else {
                        // Overwrite the user's cleartext password with hashed pw
                        cUser.password = hash;
                        next();                 // Execute the next part of chain
                    }
                });
            }
        });
    }
});

// Functions  --------------------------------------------------------------- //

/**
 * Built in functionality for us to verify the user's password with the stored
 * variant by comparing the hash of the input password compared to the expected
 * one (the one currently in the database).
 *
 * @module user
 * @function
 * @param {String} input - The password for us to check
 * @param {Object} callback - The callback / command to execute (via mongoose)
 */
UserSchema.methods.verifyPassword = function( input, callback ) {
    // Using bcrypt to comapre the input with the stored password
    bcrypt.compare( input, this.password, function(err, isMatched) {
        if(err) {
        // If an error occurs, throw error up the callback chain
            return callback(err);
        } else {
            // If successful, return no error, but do return boolean result
            callback( null, isMatched );
        }
    });
};

module.exports = mongoose.model('User', UserSchema);
