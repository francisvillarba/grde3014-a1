```
  _____                                 _          __     __
 |  ___|  _ __    __ _   _ __     ___  (_)  ___    \ \   / /
 | |_    | '__|  / _` | | '_ \   / __| | | / __|    \ \ / /
 |  _|   | |    | (_| | | | | | | (__  | | \__ \     \ V /  
 |_|     |_|     \__,_| |_| |_|  \___| |_| |___/      \_/   

 __        ___    ____      ____  _        ____   ___ ____   ___  
 \ \      / / \  |  _ \    / ___|/ |      |___ \ / _ \___ \ / _ \
  \ \ /\ / / _ \ | | | |   \___ \| |        __) | | | |__) | | | |
   \ V  V / ___ \| |_| |    ___) | |  _    / __/| |_| / __/| |_| |
    \_/\_/_/   \_\____/    |____/|_| ( )  |_____|\___/_____|\___/
                                     |/                          
```

PLEASE README :)

# GRDE3014 - Web Authoring Design



Semester 1 - Assignment 1, 2020

Created by Francis Villarba, 18266997

Contact: francis.villarba@student.curtin.edu.au

___

# Table of Contents

* Notes
* System Overview
  * Requirements
  * General Notes for Documents
  * Items
  * Users
  * Orders
* Quality Assurance

___

## NOTES

**Creation**

```
Creation of items requires the header "Content-Type": "application/json" to be added,
as well as a request body (in JSON) format to function.

You can find a copy of all the JSON I used to create the documents in the database
in "./docs/JSONs/"
```

___

## System Overview

**API ROOT URI**

Append in front of each URI string to access. *DOES NOT SUPPORT HTTPS*

```
http://[IP_ADDRESS]:8080/api
```

___

## Requirements

_Environment_

| NAME | VERSION | NOTES |
|------|---------|-------|
| Ubuntu | 18.04 |
| Cloud9 | N/A | Available from AWS |
| Node | 10.19.0 |
| NPM | 6.13.4 |
| MongoDB Shell | 4.2.3 | Must be newer than EvenNode's MongoDB version for shell to function
| OpenSSL | 1.1.1 |


_Node Dependencies_

| NAME | VERSION | NOTES |
|------|---------|-------|
| bcrypt | 4.0.1 | npm install bcrypt --save |
| body-parser | 1.19.0 |
| express | 4.17.1 |
| mongoose | 5.9.4 |
| nodemon | 2.0.2 | For Dev Mode "npm run dev" |

___

### General Notes for Documents

All documents in the collection have timestamps: true, which will auto generate
the following fields below. This is to help keep track of when an item was
created and last modified, for managerial purposes (i.e. when did we sell out an item)?

_Auto Generated fields_

| FIELD | DESCRIPTION | TYPE |
|-------|-------------|------|
| _id | The document's UUID | ObjectId |
| createdAt | When was the document created? | Date |
| updatedAt | When was the document modified / edited / updated?  | Date |

### ITEMS

**Model**

_Mongoose Schema_

| FIELD | DESCRIPTION | TYPE | REQUIRED | UNIQUE | DEFAULTS |
|-------|-------------|------|----------|--------|----------|
| name | Name of Item | String | YES | TRUE | N/A |
| altName | Alternative Names for the Item, for searching in the future | String | NO | FALSE | null |
| description | Description of the Item | String | YES | NO | NO | N/A |
| tags | For categorisation, sorting and search in the future | [String] | NO | NO | "TO BE TAGGED" |
| image | URL of images for the item, for grid / list view in the future | {String} | NO | NO | "https://upload.wikimedia.org/wikipedia/commons/d/d2/Question_mark.svg" |
| price | Price of items in various times and modes | {Schema.Types.Decimal128} | YES | NO | N/A |
| isOnSale | Is the item currently on sale, will support item view in future | {Boolean} | NO | NO | false |
| isAvailable | Is the item currently available, will allow item view to ignore item listing regardless of QTY in the future | Boolean | NO | NO | true |
| qty | The quantity of the item, will support "sold out" UX in the future view | Number | NO | NO | 0 |

_Future Considerations_

In the future, it would probably be wise to add the following fields to
support some of the more complex scenarios that a real restaurant would require

| FIELD | DESCRIPTION | REASON | TYPE | REQUIRED | UNIQUE | DEFAULTS |
|-------|-------------|--------|------|----------|--------|----------|
| ingredients | List of ingredientss for the item (if applicable) | To allow filters on the menu view, i.e. Gluten Free? Non-unique to allow items such as drinks to be included | [String] | FALSE | FALSE | "TO BE LISTED" |
| customisation | List of things we can do to modify the item, i.e. Less Chill, More Coriander | [Document] | FALSE | FALSE | null |

> Here is an example of a customisation document for an item that already has
chilli as a default ingredient. You will notice that "add" chilli is not enabled
as there is already chilli in this item. Surcharge of false means that the
POS view (in the future) will decrement the price instead of adding.
```json
{
    "ingredient": "Chilli",
    "remove": {
        "enabled": true,
        "priceChange": "true",
        "surcharge": false,
        "price": 0.25
    },
    "less": {
        "enabled": true,
        "priceChange": true,
        "surcharge": false,
        "price": 0.25
    },
    "more": {
        "enabled": true,
        "priceChange": true,
        "surcharge": true,
        "price": 0.25
    },
    "add": {
        "enabled": false,
        "priceChange": true,
        "surcharge": true,
        "price": 0.25
    }
}
```

**Routes**

_Implemented_

| NAME | FUNCTION | METHOD | URI | Variables | JSON BODY |
|------|----------|--------|-----|-----------|-----------|
| Get All Items | getItems | GET | /items/ | N/A | NO |
| Get Single Item (by id) | getItem | GET | /items/:id | ObjectId | NO |
| Create New Item | createItempost | POST | /items/create/ | N/A | YES |
| Update Item | editItemPost | PUT | /items/:id/edit | ObjectId | YES |
| Add Tags | addToArray | PUT | /items/:id/add/tag | ObjectId | YES |
| Add Alternative Names | addToArray | PUT | /items/:id/add/names | ObjectId | YES |
| Remove Tags | pullFromArray | PUT | /items/:id/remove/tag | ObjectId | NO |
| Remove Alternative Names | pullFromArray | PUT | /items/:id/remove/names | ObjectId | YES |
| Delete Item | deleteItem | DELETE | /items/:id/delete | ObjectId | NO |

_Item Editing Notes_

> Given the current implementation, you cannot use Update Item to add / or
remove items from the Tags and Alternative Names Array. This is because we need
to run a pull / push to be able to modify arrays in mongoose / mongoDB.

_Future_

Routes that are in place for future functionality, such as Views, SPAs etc, but
are not implemented. Calling them will result in a 501 - Not implemented response.

| NAME | FUNCTION | METHOD | URI | VARIABLES |
|------|----------|--------|-----|-----------|
| Item Creation View | createItemView | GET | /items/create |
| Edit Item View | editItemView | GET | /items/:id/edit | ObjectId |
| Delete Item View | deleteItemGet | GET | /items/:id/delete | ObjectId |

___

### USERS

**Model**

_Mongoose Schema_

| FIELD | DESCRIPTION | TYPE | REQUIRED | UNIQUE | DEFAULTS |
|-------|-------------|------|----------|--------|----------|
| username | Username for authentication | String | YES | YES | N/A |
| password | Hashed password for authentication | String | YES | NO | N/A |
| email | Email address for authentication, password reset and marketing | String | YES | YES | N/A |
| isEmailValidated | Was the email validated? Not required for built in system accounts | Boolean | NO | NO | false |
| name | The full name of the user | String | YES | NO | N/A |
| mobile | The mobile number for the user, to txt when food is ready for collection | String | NO | NO | null |
| marketing | For GDPR, do they consent to marketing emails? | Boolean | NO | NO | false |
| lastLogin | When did the user last login to the system | Date | NO | NO | Date.now |
| privileges | What access does the user have to the system, for future use | String | NO | NO | null |
| orderHistory | ObjectIds that correspond to Order document Ids in the orders collection | [Schema.Types.ObjectId] | FALSE | FALSE | N/A |

_Privileges Notes_

> Using string for now as it will allow for multiple privileges, for more
granular access control in the future. In the next revision, this will be replaced
with a privileges document for consistency in the future.

>Below is an example privileges document for a manager in the future.
```json
{
    "banned": false,
    "user": true,
    "pos": true,
    "kitchen": true,
    "manager": true,
    "owner": false
}
```

_Middleware_

This model contains the bcrypt middleware to allow for the encryption of the
user's password. Upon creation of the user document, the password is
encrypted and replaced with the hashed representation before being saved to
the user's collection in the database.

For your convenience, an API call has been added to check if
bcrypt is working correctly.

**Routes**

_Implemented_

| NAME | FUNCTION | METHOD | URI | VARIABLES |
|------|----------|--------|-----|-----------|
| Get All Users | listUsers | GET | /users/ |
| Get Single User (by id) | listUser | GET | /users/:id | ObjectId |
| Create New User | createUser | POST | /users/create |
| Verify Password (DEBUG ONLY) | verifyLogin | GET | /users/:id/verify/:pw | ObjectId, Password |
| Update User | editUser | PUT | /users/:id/edit | ObjectId |
| Update User Password | changePassword | PUT | /users/:id/edit/password/ | ObjectId |
| Delete User | deleteUser | DELETE | /users/:id/delete/ | ObjectId |
| Delete User (Verify) | deleteUserVerified | DELETE | /users/:id/delete/:pw | ObjectId, Password |

_Verify Password_

This is an additional function added, purely for debugging purposes. This is
to allow us to check whether the password has successfully been encrypted
and that the known password matches the hashed representation using bcrypt's
compare method.

_Update User Password_

This additional function was added as a work-around to an issue whereby Mongoose
does not fire .pre functionality when using ES6 arrow => functions.

> For example, if one were to update a user's password using the standard
Update User API call, the password would not be encrypted as .pre is not fired,
thus, causing the password to be stored in plain-text. This work around ensures
updates to the user's password is encrypted and stored as a hash.

_Not Implemented_

Routes that were planned, but were not implemented due to various factors such
as time restraints.

| NAME | FUNCTION | METHOD | URI | VARIABLES | JSON |
|------|----------|--------|-----|-----------|------|
| Add to Order History | addToOrderHistory | POST | /users/:id/add/history | ObjectId | YES |
| Edit Order History | editOrderHistory | PUT | /users/:id/edit/history/:hid | ObjectId, ObjectId | YES |

> These methods are to allow for addition and removal of orders to the order
history array using pull and push mongoose methods. This would have
prevented a scenario of accidentally clearing the entire order history list


_Future Considerations_

Routes that are in place for future functionality. Calling these will currently
result in a 501 - Not Implemented response.

| NAME | FUNCTION | METHOD | URI | VARIABLES |
|------|----------|--------|-----|-----------|
| Create User View | createUserForm | GET | /users/create |
| Edit User View | editUserForm | GET | /users/:id/edit | ObjectId |
| Delete User View | deleteUserForm | GET | /users/:id/delete | ObjectId |

___

### Orders

**Model**

_Mongoose Schema_

Orders are separated into primary and secondary documents, this allows flexibility
for future features and requirements and a clear separation of what each part
of the order document is dealing with.

> OrderSchema | The main schema for Order documents

| FIELD | DESCRIPTION | TYPE | REQUIRED | UNIQUE | DEFAULTS |
|-------|-------------|------|----------|--------|----------|
| user | who ordered it? | Schema.Types.ObjectId | YES | NO | N/A |
| target | What type of order is it? | Number | YES | NO | N/A |
| scheduledFor | For scheduled order targets, what time is it due? | Date | NO | NO | Date.now + 60000 |
| status | What is the status of the order, for tracking | Number | NO | NO | 0 |
| isNotified | Have we notified the customer of completion yet? | Boolean | NO | NO | false |
| isCompleted | Is the order fully completed, i.e. paid, cooked and collected? | NO | NO | false |
| items | The items associated with this order | [OrderItemsSchema] |
| payment | The payment information for the order | {PaymentInfoSchema} |
| isPaid | Is the order paid? false == pending payment | Boolean | NO | NO | false |

Target Codes for Orders

| CODE | MEANING / REPRESENTATION |
|------|--------------------------|
| 1 | Dine In |
| 2 | Take Away |
| 3 | Delivery |
| 4 | Scheduled Order - Dine In |
| 5 | Scheduled Order - Take Away |
| 6 | Scheduled Order - Delivery |
| 7-9 | Future Use

Status Codes for Orders

| CODE | MEANING / REPRESENTATION |
|------|--------------------------|
| 0 | Payment Required / Not Paid Yet |
| 1 | In Progress / Cooking / Preparing Order |
| 2 | Ready for Collection |
| 3 | Completed |
| 4 | Problem with Order |
| 5-9 | Future Use |

> OrderNotesSchema | Sub-Document for OrderItems documents

| FIELD | DESCRIPTION | TYPE |
|-------|-------------|------|
| flag | What type of note is this? | Number |
| comment | The body content of the note | String |

Flag Codes for Notes

| CODE | MEANING / REPRESENTATION |
|------|--------------------------|
| 1 | Staff Discount |
| 2 | Discount (General) |
| 3 | Promotion |
| 4 | Refund |
| 5 | Exchange (Exchanged something) |
| 6 | IngredientChange |
| 7-9 | Future Use |

> OrderItemsSchema | Sub-Document for Order documents, that represent each item

| FIELD | DESCRIPTION | TYPE | REQUIRED | UNIQUE | DEFAULTS |
|-------|-------------|------|----------|--------|----------|
| id | The ObjectId of the item document in the items collection | Schema.Types.ObjectId | YES | NO | N/A |
| qty | The quantity of the item to be ordered | Number | NO | NO | 1 |
| listPrice | The price of the item, as it was at the time of order | Schema.Types.Decimal128 | YES | NO | N/A |
| finalPrice | The price of the item, after changes due to discount etc. | Schema.Types.Decimal128 | YES | NO | N/A |
| notes |  Any notes associated with this item | [OrderNotesSchema] | NO | NO | null |

List price and final price was added to ensure and maintain the consistency of
the order history. This means that, Should the prices of items change in the future,
but we wanted to have a view that showed historical trend of sales and profit
for a particular item prior to price increase, we can do so.

Furthermore, the price differential between list and final price can be used
to reconcile sales at the end of the day, as this differential can be used
to calculate the discounts given on the day and by how much.

> PaymentInfoSchema | Sub-Document for Payment Information for an Order

| FIELD | DESCRIPTION | TYPE | REQUIRED | UNIQUE | DEFAULTS |
|-------|-------------|------|----------|--------|----------|
| method | What did they use to pay? | Number | NO | NO | 0 |
| totalPrice | The final price of the order | Schema.Types.Decimal128 | NO | NO | 0.00 |
| processedAt | When was the order paid? | Date | NO | NO | null |
| processor | Which user processed the order? | Schema.Types.ObjectId | NO | NO | null |

Method Codes for Payment

| NUMBER | MEANING / REPRESENTATION |
|--------|--------------------------|
| 0 | Not Paid / Unpaid |
| 1 | Cash |
| 2 | Card |
| 3 | AMEX |
| 4 | AliPay |
| 5-9 | Future Use |

Payment method, date paid and who processed the payment at the POS are all there
to help with sales reconciliation at the end of the day. Should issues or
discrepancies occur, the processedAt and processor information can be used
to check cameras / security for investigation.

Total Price is initially 0.00 as this would be calculated at the end of
the step, just in case there is discounts etc. applied to the order
before payment (due to offers, etc).

**Routes**

_Implemented_

| NAME | FUNCTION | METHOD | URI | VARIABLES | JSON BODY |
|------|----------|--------|-----|-----------|-----------|
| Get All Orders | getOrders | GET | /orders/ | NONE | NO |
| Get Single Order (by id) | getOrder | GET | /orders/:id | ObjectId | NO |
| Create New Order | createOrder | POST | /orders/create | NONE | YES |
| Edit Order | editOrder | PUT | /orders/:id/edit/ | ObjectId | YES |
| Add Item to Order | addItem | POST | /orders/:id/add/item | ObjectId | YES |
| Add Item Notes | addItemNote | POST | /orders/:id/add/note/:item | ObjectId, ObjectId | YES |
| Remove Item from order | removeItem | DELETE | /orders/:id/delete/item/:item | ObjectId, ObjectId | NO |
| Remove Item Note | removeItemNote | DELETE | /orders/:id/delete/note/:item/:note | ObjectId, ObjectId, ObjectId | NO |
| Clear Item Notes | clearItemNotes | DELETE | /orders/:id/delete/notes/:item | ObjectId, ObjectId | NO |
| Delete Order | deleteOrder | DELETE | /orders/:id/delete | ObjectId | NO |

_Notes and Items_

>Notes and Items in this order are documents. In the grand scheme of things, this means
that Notes and items are sub-documents to the Order document. Thus, delete and post
methods are used as they better reflect what is being done (rather than using PUT).
See more in the order model located in ./models/order.js

_Future Routes_

Routes that are in place for future functionality. Calling these will currently
result in a 501 - Not Implemented response.

| NAME | FUNCTION | METHOD | URI | VARIABLES | JSON |
|------|----------|--------|-----|-----------|------|
| Order Creation View | createOrderView | GET | /orders/create | NONE | YES |
| Edit Order View | editOrderView | GET | /orders/:id/edit | ObjectId | YES |
| Delete Order View | deleteOrderView | GET | /orders/:id/delete | ObjectId | NO |

**Notes**

In the scenario that the customer is a walk-in customer and orders without
an account, we can use the NO Account user instead
