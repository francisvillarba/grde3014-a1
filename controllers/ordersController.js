'use strict';

/**
 * ordersController.js.
 *
 * The controller that deals with orders. Responds with relevant
 * data, based on routed information from the users.
 *
 * Inspired by https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/routes
 *
 * @author  Francis Villarba <francis.villarba@student.curtin.edu.au>
 * @version 2.0.1
 *
 * @requires nodejs
 * @requires mongodb
 * @requires express
 * @requires mongoose
 */

// Setup -------------------------------------------------------------------- //
var Order = require('../models/order');    // The main model we require

// Private Helper Methods --------------------------------------------------- //

// Listing ------------------------------------------------------------------ //

/**
 * Obtains a list of all order documents in the mongoDB "orders" collection.
 * @module ordersController
 * @function
 * @param {Object} req - The request, should be empty in this instance
 * @res {Object} res - The response
 * @return {Undefined}
 */
exports.getOrders = (req, res) => {
    // For debugging
    console.log(`Getting all orders from the Orders collection`);

    // Run the mongoDB command
    Order.find({})
    .then( (orders) => {
        if( orders.length == 0 ) {
            // There is no orders on the database yet!
            res.status(404).send({
                msg: 'No orders found'
            });
        } else {
            // Respond with the list of all orders
            res.json(orders);
        }
    })
    .catch( (err) => {
        // Looks like we've encountered an issue
        console.log(err);                                       // For debugging
        res.status(500).send({
            msg: 'orderController.getOrders | There was a problem processing your request',
            error: err.message
        });
    });
};

/**
 * Returns a single order, based on the given MongoDB Object_Id
 * @module ordersController
 * @function
 * @param {Object} req - The request
 * @param {String} req.params.id - The MongoDB Object_Id
 * @param {Object} res - The response
 * @return {Undefined}
 */
exports.getOrder = (req, res) => {
    // For debugging
    console.log(`Getting Order "${req.params.id}"`);

    // Run the MongoDB command
    Order.findById(req.params.id)
    .then( (result) => {
        // If we found an item, send response
        res.json(result);
    })
    .catch( (err) => {
        // DRATS! An eror has occured!
        console.log(err);                                       // For debugging
        if( err.kind == 'ObjectId' ) {
            // If the error is related to the user supplied ObjectId
            res.status(404).send({
                msg: `Order with ID "${req.params.id}" was not found`
            });
        } else {
            // An uncaught error has occured, quick! Server issue!
            res.status(500).send({
                msg: `orderController.gerOrder | There was a problem processing your request`,
                error: err.message
            });
        }
    });
};

// Creation ----------------------------------------------------------------- //

// TODO - FUTURE - Create an order | Getting the creation form
exports.createOrderView = (req, res) => {
    console.log(`Call made to not implemented function | ordersController.createOrderView`);
    res.status(501).send('Not Implemented: orderController.orders_create_get');
};

/**
 * Creates a new Order doucment in the MongoDB orders collection
 * @module ordersController
 * @param {Object} req - The request
 * @param {Object} req.body - User supplied json with info about the new order
 * @param {Object} res - The response
 * @return {Undefined}
 */
exports.createOrder = (req, res) => {
    // For debugging
    console.log(`Creating new order`);

    // Validate the request
    if(!req.body) {
        console.log(`Was given empty body! Aborting Order Creation!!!`);
        res.status(400).send({
            msg: 'The order content and information cannot be empty!'
        });
    } else { // Create the Order document
        // For debugging
        // console.log(req.body);
        // res.send('body receieved');

        let newOrder = new Order(req.body);     // Create an instance variable
        newOrder.save()                         // Attempt to save in MongoDB
        .then( (order) => {
            // Return the json with details of the newly created order
            res.status(201).json(order);
        })
        .catch( (err) => {
            console.log(err);                   // For debugging, log the error
            res.status(500).send({
                msg: 'ordersController.createOrder | Could not create order!',
                error: err.message
            });
        });
    }
};

// Editing ------------------------------------------------------------------ //

// TODO - FUTURE - Edit an order | Getting the edit form
exports.editOrderView = (req, res) => {
    console.log(`Call made to not implemented function | ordersController.editOrderView`);
    res.status(501).send('Not Implemented: orderController.orders_edit_get');
};

/**
 * Editing an order of Object_id with json
 *
 * !!NOTE!!
 * This function does not work with some aspects of the order schema,
 * to add items to the items array, or add notes to an order, please
 * use the functions such as addItemNote, removeItemNote etc.
 *
 * @module ordersController
 * @function
 * @param {Object} req - The request
 * @param {String} req.params.id - The Object_id of the order to edit
 * @param {Object} req.body - The json containing information to modify
 * @param {Object} res - The response
 * @return {Undefined}
 */
exports.editOrder = (req, res) => {
    // For debugging
    console.log(`Editing order "${req.params.id}"`);

    if(!req.body) { // If we are given an empty request body
        console.log(`Was given empty body! Aborting Order Edit!!!`);
        res.status(400).send({
            msg: 'The order modification content cannot be empty!'
        });
    } else { // Edit the item
        // Set the new values
        Order.findByIdAndUpdate( req.params.id, req.body, { new: true } )
        .then( (result) => {
            // Print out results (which is updated document)
            res.status(200).json(result);
        })
        .catch( (err) => {
            // An error we didn't expect occured!
            console.log(err);                   // For debugging, log the error
            res.status(500).send({
                msg: `ordersController.editOrder | Could not edit order of id "${req.params.id}"`,
                error: err.message
            });
        });
    }
};

/**
 * Adding a note to an item
 *
 * @module ordersController
 * @function
 * @param {Object} req - The request
 * @param {String} req.params.id - The Order_Id
 * @param {String} req.params.item - The Object_Id of the OrderItem we are noting
 * @param {Object} req.body - The notes we are adding, as a json object
 * @param {Object} res - The response
 * @return {Undefined}
 */
exports.addItemNote = (req, res) => {
    // For debugging
    console.log(`Adding note to Item "${req.params.item}" from Order "${req.params.id}"`);

    // Check the variables
    if(!req.body) {
        console.log(`Was given empty body! Aborting Item Note Addition!!!`);
        // If the body of the request was empty, let them know
        res.status(400).send({
            msg: `The notes body cannot be empty!`
        });
    } else {
        // For debugging
        // console.log(req.body);
        // console.log(`Editing Notes for Order _id ${req.params.id}`);
        // console.log(`Adding Notes for Item with _id ${req.params.item}`);

        // The selection query will move the $ selector to where we need it
        let selectionQuery = {
            // Set the selector $ to the Order document
            _id: `${req.params.id}`,
            "items._id": `${req.params.item}`
            // Then set selector $ to the item with that id
        };

        // Push to the selected item's notes array with data from req.body
        let pushQuery = {
            $push: {
                "items.$.notes": req.body
            }
        };

        // Actually run the commands on the database
        Order.update( selectionQuery, pushQuery, { new: true } )
        .then( (result) => {
            // Prints out the results
            res.status(201).json(result);
        })
        .catch( (err) => {
            // An error occured!
            console.log(err);                                   // For debugging
            res.status(500).send({
                msg: `ordersController.addItemNote | Could not add note to Order_id "${req.params.id}" with target Item_id "${req.params.item}"`,
                error: err.message
            });
        });
    }
};

/**
 * Removes a note from an item
 *
 * @module ordersController
 * @function
 * @param {Object} req - The request
 * @param {String} req.params.id - The Order's Object_Id
 * @param {String} req.params.item - The Object_id of the OrderItem we are editing
 * @param {String} req.params.note - The Object_id of the OrderNote we are removing
 * @param {Object} res - The response
 * @return {Undefined}
 */
exports.removeItemNote = (req, res) => {
    // For debugging
    console.log(`Removing Note "${req.params.note}" from Item "${req.params.item}" from Order "${req.params.id}"`);

    // Check the variables
    if(!req.body) {
        console.log(`Was given empty body! Aborting Note Removal!!!`);
        // If the body of the request was empty, let them know
        res.status(400).send({
            msg: `The notes body cannot be empty!`
        });
    } else {
        // For debugging
        // console.log(req.body);
        // console.log(`Pulling Notes for Order ${req.params.id}`);
        // console.log(`Targeting Notes for Item with Index ${req.params.index}`);


        // The selection query will set the selector $ to where we need it
        let selectionQuery = {
            // Set the selector $ to the order document
            _id: `${req.params.id}`,
            // Set the selector $ to the item with the corresponding ID
            "items._id": `${req.params.item}`
        };

        // The query where we pull the item of a given note
        let pullQuery = {
            $pull: {
                "items.$.notes": {
                    _id: `${req.params.note}`
                }
            }
        };

        // Now we run the commands on the database
        Order.update( selectionQuery, pullQuery, { new: true } )
        .then( (result) => {
            // Prints out the results
            res.status(200).json(result);
        })
        .catch( (err) => {
            // An error occured!
            console.log(err);
            if( err.kind == 'ObjectId' ) {
                // Cannot find either the Order or Item requested
                res.status(404).send({
                    msg: `Order or Item not found`,
                    error: err.message
                });
            } else {
                // General Error, Throw server error
                res.status(500).send({
                    msg: `ordersController.removeItemNote | A problem occured when attempting to remove a note from Order_id "${req.params.id}"" with the target Item_id "${req.params.item}"`,
                    error: err.message
                });
            }
        });
    }
};

/**
 * Clears all notes associated with a given order's item
 *
 * @module ordersController
 * @function
 * @param {Object} req - The request
 * @param {String} req.params.id - The Order's Object_Id
 * @param {String} req.params.item - The Object_id of the OrderItem we are editing
 * @param {Object} res - The response
 * @return {Undefined}
 */
exports.clearItemNotes = (req, res) => {
    // For debugging
    console.log(`Clearing notes for Item "${req.params.item}" from Order "${req.params.id}"`);

    // Strip the notes from the order
    // Use lean as we don't actually need the output to speed up MongoDB processing speed
    Order.findById( req.params.id ).lean()             // Find if it exists first
    .then( (result) => {
        // For debugging
        // console.log(result);
        // console.log(result.items.length);

        // Check if the Item exists
        let itemExists = Order.find({}).where({ "items._id": `${req.params.item}`}).lean().count()
        .then( (result) => {
            return result;
        });
        // No need to catch error on OrderId as we have already checked this earlier

        if(itemExists) {
            // Clear the notes attached to the item
            Order.update({
                // Set our selector to our item
                _id: `${req.params.id}`,
                "items._id": `${req.params.item}`
            },
            {
                // Unsure if this is necessary in NoSQL databases
                // By using set, we are making the item's [OrderNotes] empty to ensure no dangling records
                $set: {
                    "items.$.notes": []
                }
            })
            .then( (result) => {
                // After we update, send a response back to the user
                res.status(200).send({
                    msg: `Successfully cleared notes for Item "${req.params.item}" from order "${req.params.id}"`
                });
            })
            .catch( (err) => {
                // An error occured while trying to access the item's notes, deal with it here
                console.log(err);
                res.status(500).send({
                    msg: `ordersController.removeItem | There was a problem processing your request`,
                    error: err.message
                });
            });
        } else {
            // Specified item does not exist in this order
            console.log(`Item ""${req.params.item}" does not exist in Order "${req.params.id}"`);
            res.status(404).send({
                msg: `ordersController.clearItemNotes | Could not find Item "${req.params.item}" in Order "${req.params.id}"`
            });
        }
    })
    .catch( (err) => {
        console.log(err);                                       // For debugging
        // An error occured
        if( err.kind == 'ObjectId' ) {
            // If it is related to the Order's ObjectId not being found
            res.status(404).send({
                msg: `ordersController.clearItemNotes | Could not find Order "${req.params.id}"`
            });
        } else {
            // A more generalised error has occured, assume server issue
            res.status(500).send({
                msg: `ordersController.clearItemNotes | There was a problem processing your request`,
                error: err.message
            });
        }
    });
};

/**
 * Adding an item to the order
 *
 * @module ordersController
 * @function
 * @param {Object} req - The request
 * @param {String} req.params.id - The object_id of the order to edit
 * @param {Object} req.body - The json containing information about the item to add to the order
 * @param {Object} res - The response
 * @return {Undefined}
 */
exports.addItem = (req, res) => {
    // For debugging
    console.log(`Adding Item "${req.params.item}" to Order "${req.params.id}"`);

    if(!req.body) {
        console.log(`Was given empty body! Aborting Item Addition!!!`);
        res.status(400).send({
            msg: 'The item body content cannot be empty!'
        });
    }
    else { // Add the item
        // For debugging
        // console.log(`Adding items for Order ${req.params.id}`);
        // console.log(req.body);

        // Talk to the mongoDB
        Order.findByIdAndUpdate( req.params.id, {
            // We push to the Order's [OrderItems] field
            $push: {
                "items": req.body
            }
        },
        {
            // Get output of our newly modifed Order Document
            new: true
        })
        .then( (result) => {
            // Give the newly modified Order Document to the user
            res.status(201).json(result);
        })
        .catch( (err) => {
            console.log(err);                                   // For debugging
            if( err.kind == 'ObjectId' ) {
                // The order doesn't exist
                res.status(404).send({
                    msg: `ordersController.addItemToOrder | Could not find the specified order to add items to`,
                });
            } else {
                // Server related issue occured?
                res.status(500).send({
                    msg: `ordersController.addItemToOrder | There was a problem processing your request`,
                    error: err.message
                });
            }
        });
    }
};

/**
 * Removes all items from the order based on a given itemId
 *
 * @module ordersController
 * @function
 * @param {Object} req - The request
 * @param {String} req.params.id - The object_id of the order to modify
 * @param {String} req.params.item - The object_id of the item to remove
 * @param {Object} res - The response
 * @return {Undefined}
 */
exports.removeItem = (req, res) => {
    // For debugging
    console.log(`Removing Item "${req.params.item}" from Order "${req.params.id}"`);

    // Strip the notes from the order
    // Use lean as we don't actually need the output to speed up MongoDB processing speed
    Order.findById( req.params.id ).lean()             // Find if it exists first
    .then( (result) => {
        // For debugging
        // console.log(result);
        // console.log(result.items.length);

        // Check if the Item exists
        let itemExists = Order.find({}).where({ "items._id": `${req.params.item}`}).lean().count()
        .then( (result) => {
            return result;
        });
        // No need to catch error on OrderId as we have already checked this earlier

        if(itemExists) {
            // Clear the notes attached to the item
            Order.update({
                // Set our selector to our item
                _id: `${req.params.id}`,
                "items._id": `${req.params.item}`
            },
            {
                // Unsure if this is necessary in NoSQL databases
                // By using set, we are making the item's [OrderNotes] empty to ensure no dangling records
                $set: {
                    "items.$.notes": []
                }
            })
            .catch( (err) => {
                // An error occured while trying to access the item's notes, deal with it here
                console.log(err);
                res.status(500).send({
                    msg: `ordersController.removeItem | There was a problem processing your request`,
                    error: err.message
                });
            });

            // Pull the item from the Order Items Sub-Document Array
            Order.update({
                _id: `${req.params.id}`
            },
            {
                // Pull the item from order's [OrderItems]
                $pull: {
                    "items" : {
                        _id: `${req.params.item}`
                    }
                }
            },
            {
                new: true
            })
            .then( (result) => {
                // Return a copy of the newly modified Order
                res.status(200).json(result);
            })
            .catch( (err) => {
                // An error occured while trying to pull the item from the order, deal with it here
                console.log(err);
                res.status(500).send({
                    msg: `ordersController.removeItem | There was a problem processing your request`,
                    error: err.message
                });
            });
        } else {
            // Item doesn't exist, throw an error (!itemExists)
            console.log(`Item "${req.params.item}" does not exist in Order "${req.params.id}"`);
            res.status(404).send({
                msg: `ordersController.removeItem | Could not find OrderItem "${req.params.item}" in Order ${req.params.id}`
            });
        }
    })
    .catch( (err) => {
        console.log(err);                                       // For debugging
        if( err.kind == 'ObjectId' ) {
            // If the error is related to the Order's ObjectId
            res.status(404).send({
                msg: `ordersController.removeItem | Could not find Order "${req.params.id}"`
            });
        } else {
            // A general error occured that we haven't accounted for
            res.status(400).send({
                msg: `ordersController.removeItem | There was a problem processing your request`,
                error: err.message
            });
        }
    });
};

// Deletion ----------------------------------------------------------------- //

// TODO - FUTURE - Delete an order | Getting the deletion form
exports.deleteOrderView = (req, res) => {
    console.log('Call made to not implemented function | ordersController.deleteOrderView');
    res.status(501).send('Not Implemented: ordersController.deleteOrderView');
};

/**
 * Deletes an order
 *
 * @module ordersController
 * @function
 * @param {Object} req - The request
 * @param {String} req.params.id - The object_id of the order to modify
 * @param {Object} res - The response
 * @return {Undefined}
 */
exports.deleteOrder = (req, res) => {
    // For debugging
    console.log(`Deleting order "${req.params.id}"`);

    // Check if the order exists first
    Order.findById( req.params.id )
    .then( (result) => {

        // Check some variables
        let isPaid = result.isPaid;
        let isCompleted = result.isCompleted;
        let status = result.status == 0;

        // For debugging
        // console.log(result);
        // console.log(isPaid);
        // console.log(isCompleted);
        // console.log(status);

        if(!isPaid && !isCompleted && status) {
            // If not paid, not completed and status is zero (PaymentRequired)

            // Safe to delete
            Order.findByIdAndRemove( req.params.id )
            .then( (result) => {
                res.status(200).send({
                    msg: `Order "${req.params.id}" was deleted`
                });
            })
            .catch( (err) => {
                console.log(err);                               // For debugging
                // An error has occured
                if( err.kind == 'ObjectId' ){
                    // The order doesn't exist
                    res.status(404).send({
                        msg: `ordersController.deleteOrder | Order "${req.params.id}" was not found!`
                    });
                } else {
                    res.status(500).send({
                        msg: `orderController.deleteOrder | There was a problem processing your request`,
                        error: err.message
                    });
                }
            });
        } else {
            /*
             * It is not safe to delete as deletion can have an effect on order
             * history integrity, causing problems for accounting. Deleting the
             * order will cause EFTPOS Merchant totals and POS Cash would be
             * incorrect and WILL cause headaches when doing accounting.
            */
            res.status(400).send({
                msg: `ordersController.deleteOrder | Could not delete Order "${req.params.id}"`,
                notice: "Order has already been paid and / or processed. \n Deleting this order will result in errors in order history and sales reconciliation!"
            });
        }
    })
    .catch( (err) => {
        // An error has occured
        console.log(err);                                       // For debugging
        if( err.kind == 'ObjectId' ){
            // The order doesn't exist
            res.status(404).send({
                msg: `ordersController.deleteOrder | Order "${req.params.id}" was not found!`
            });
        } else {
            res.status(500).send({
                msg: `orderController.deleteOrder | There was a problem processing your request`,
                error: err.message
            });
        }
    });
};
