'use strict';

/**
 * userController.js.
 *
 * The controller that deals with users and user accounts. Responds with relevant
 * data, based on routed information from the users.
 *
 * Inspired by https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/routes
 *
 * @author  Francis Villarba <francis.villarba@student.curtin.edu.au>
 * @version 2.1.3
 *
 * @requires nodejs
 * @requires mongodb
 * @requires express
 * @requires mongoose
 * @requires bcrypt
 */

// Setup  ------------------------------------------------------------------- //
const Users = require('../models/user');                      // The User Schema

// Using bcrypt encryption for safe password storage!
const bcrypt = require('bcrypt');                           // For Encryption
const SALT_WORK_FACTOR = 10;                                // Encryption Factor

// Listing ------------------------------------------------------------------ //

/**
 * Display a list of all users in the database
 *
 * @module usersController
 * @function
 * @param {Object} req - The request
 * @param {Object} res - The response
 * @return {Undefined} - NO returns from this method
 */
exports.listUsers = (req, res) => {
    // For debugging
    console.log(`Listing all users in the users collection`);

    // Ideally we should authenticate the user as an admin before running xD

    // Run Mongoose Commands
    Users.find({})
    .then( (result) => {
        if( result.length == 0 ) {
            // We didn't find any users in the database
            res.status(404).send({
                msg: `No users found!`
            });
        } else {
            // We found users, let's display them
            res.status(200).json(result);
        }
    })
    .catch( (err) => {
        // An error has occured, likely communication or server related
        console.log(err);
        res.status(500).send({
            msg: `usersController.listUsers | There was a problem processing your request`,
            error: err.message
        });
    });
};

/**
 * Display a single user from the database
 *
 * @module usersController
 * @function
 * @param {Object} req - The request
 * @param {String} req.params.id - The MongoDB ObjectId for the user we are finding
 * @param {Object} res - The response
 * @return {Undefined} - No data is returned if method is called directly
 */
exports.listUser = (req, res) => {
    // For debugging
    console.log(`Listing User "${req.params.id}"`);

    // Like listUsers, ideally we need to authenticate before allowing this function
    // Run Mongoose commands
    Users.findById( req.params.id )
    .then( (result) => {
        // We found the book, return a json response with the book's information
        res.status(200).json(result);
    })
    .catch( (err) => {
        // An error has occured
        console.log(err);
        if( err.kind == 'ObjectId' ) {
            // If the error is related to the ObjectID
            res.status(404).send({
                msg: `The user with ID "${req.params.id}" could not be found`
            });
        } else {
            // A more generalised error, assume server error
            res.status(500).send({
                msg: `usersController.listUser | There was a problem processing your request`,
                error: err.message
            });
        }
    });
};

// Creation ----------------------------------------------------------------- //

// Create a user | Getting the creation form
exports.createUserForm = (req, res) => {
    console.log('Call made to not implemented function | usersController.createUserForm');
    res.status(501).send('Not Implemented: userController.users_create_get');
};

/**
 * Creates a user in the database
 *
 * @module usersController
 * @param {Object} req - The request
 * @Param {Object} req.body - The body of the request in application/json format
 * @param {Object} res - The response
 * @return {Undefined}
 */
exports.createUser = (req, res) => {
    // For debugging
    console.log(`Creating new user`);

    if(!req.body) {
        // We cannot have an empty body tag
        console.log("Was given empty body! Aborting User Creation!!!");
        res.status(400).send({
            msg: `The user content body cannot be empty!`
        });
    } else {
        // Convert from json to js object
        let newUser = new Users(req.body);

        // Time to save!
        newUser.save()
        .then( (result) => {
            // If all was successful, return a copy of our new user account
            res.status(201).json(result);
        })
        .catch( (err) => {
            // Oh dear! Looks like an error has occured
            console.log(err);
            res.status(500).send({
                msg: `Could not create user`,
                error: err.message
            });
        });
    }
};

// Verification ------------------------------------------------------------- //

/**
 * Verifies the login information
 *
 * Technically, this is a security vulnerability as one could fesably run a
 * script to run through usernames and passwords until it finds a match... Would
 * be wise to put a delay when it is unsuccesful.
 *
 * @module usersController
 * @function
 * @param {Object} req - The request
 * @param {String} req.params.id - The username
 * @param {String} req.params.pw - The password
 * @param {Object} res - The response
 * @return {Undefined}
 */
exports.verifyLogin = (req, res) => {
    // For debugging
    console.log(`Verifying credentials for "${req.params.id}`);

    // // Actually verify the password here
    Users.findOne( { _id: `${req.params.id}` }, (err, result) => {
        if(err) {
            // An error has occured
            console.log(err);
            res.status(500).send({
                msg: `usersController.verifyLogin | There was a problem processing your request`,
                error: err.message
            });
        } else {
            // Let's verify the password
            result.verifyPassword( req.params.pw, (err, vResult) => {
                // Let's check the callback variables
                if(err) {
                    // An error has occured
                    console.log(err);
                    // A more detailed error explanation
                    res.status(500).send({
                        msg: `usersController.verifyLogin | There was a problem processing your request`,
                        where: `Password verification`,
                        error: err.message
                    });
                } else {
                    // Let's check the result
                    if(vResult) {
                        // If it is valid
                        res.status(200).send({
                            msg: `Success`
                        });
                    } else {
                        // If not valid
                        res.status(400).send({
                            msg: `Unsuccessful`
                        });
                    }
                }
            });
        }
    });
};

// Modification Calls ------------------------------------------------------- //

// TODO - FUTURE Edit a user | Getting the edit form
exports.editUserForm = (req, res) => {
    console.log('Call made to not implemented function | usersController.editUserForm');
    res.status(501).send('Not Implemented: userController.users_edit_get');
};

/**
 * Edit / Update the user's information
 *
 * DO NOT USE THIS METHOD TO UPDATE THE USER'S PASSWORD. THIS WILL NOT WORK
 * AS MONGOOSE DOES NOT SUPPORT / FIRE PRE METHODS ON ANYTHING OTHER THAN .SAVE;
 * THIS MEANS THAT FUNCTIONS LIKE FINDBYIDANDUPDATE WILL NOT RE-ENCRYPT THE
 * PASSWORD AND WILL INSTEAD KEEP IT IN PLAIN TEXT.
 *
 * @module usersController
 * @function
 * @param {Object} req - The request
 * @param {String} req.params.id - The id of the user to modify
 * @param {Object} req.body - The body that contains the edits in json format
 * @param {Object} res - The response
 * @return {Undefined}
 */
exports.editUser = (req, res) => {
    // For debugging
    console.log(`Modifying user "${req.params.id}"`);

    if( !req.body ) {
        // If the body of the request is empty, we need to say something!
        console.log('Was given empty body! Aborting User Edit!!!');
        res.status(400).send({
            msg: `The user body content cannot be empty!`
        });
    } else {
        Users.findByIdAndUpdate( req.params.id, req.body, { new: true })
        .then( (result) => {
            // If all is well, let's return a copy of the newly modified entry
            res.status(200).json(result);
        })
        .catch( (err) => {
            // Log the error
            console.log(err);

            if( err.kind == 'ObjectId' ) {
                // We can't find the user
                res.status(404).send({
                    msg: `usersController.editUser | Could not find user with id "${req.params.id}"`
                });
            } else {
                // A more generalised / uncaught error has occured
                res.status(500).send({
                    msg: `usersController.editUser | There was a problem processing your request`,
                    error: err.message
                });
            }
        });
    }
};

/**
 * Change the user's password (the alternative edit method) that will ensure
 * the password is re-encrypted
 *
 * @module usersController
 * @function
 * @param {Object} req - The request
 * @param {String} req.parmas.id - The id of the user to modify
 * @param {Object} req.body - The body that contains the old and new password
 * @param {String} req.body.old - The old password for the user
 * @param {String} req.body.new - The new password for the user
 * @param {Object} res - The response
 * @return {Undefined}
 */
exports.changePassword = (req, res) => {
    // For debugging
    console.log(`Changing the password for user "${req.params.id}"`);

    if( !req.body ) {
        // The body needs to contain both the old and new password!!!
        console.log('Was given empty body! Aborting Password Change!!!');
        res.status(400).send({
            msg: `The user body content cannot be empty!`
        });
    } else {
        // TODO - FUTURE - Verify the password before allowing the change
        // Omitted for now as I want to focus on MVP (Minimum viable product)

        // For debugging
        // console.log(`Hashing password`);

        bcrypt.hash( req.body.new, SALT_WORK_FACTOR)
        .then( (result) => {
            // For debugging
            // console.log(result);

            // Setup our new request
            let updateJson = { "password": result };

            // Then find on the database to change
            Users.findByIdAndUpdate( req.params.id, updateJson, { new: true } )
            .then( (updateResult) => {
                res.status(200).send({
                    msg: `Password change for user "${req.params.id}" was successful`
                });
            })
            .catch( (err) => {
                // Log the error
                console.log(err);

                if( err.kind == 'ObjectId' ) {
                    // We can't find the user
                    res.status(404).send({
                        msg: `usersController.changePassword | Could not find user with id "${req.params.id}"`
                    });
                } else {
                    // A more generalised / uncaught error has occured
                    res.status(500).send({
                        msg: `usersController.changePassword | There was a problem processing your request`,
                        error: err.message
                    });
                }
            });
        })
        .catch( (err) => {
            // A more generalised / uncaught error has occured
            console.log(err);
            res.status(500).send({
                msg: `usersController.changePassword | There was a problem processing your request`,
                error: err.message
            });
        });
    }
};

// Deletion ----------------------------------------------------------------- //

// TODO - FUTURE - Delete an order | Getting the deletion form
exports.deleteUserForm = (req, res) => {
    console.log('Call made to not implemented function | usersController.deleteUserForm');
    res.status(501).send('Not Implemented: userController.users_delete_get');
};

/**
 * Delete a user from the users collection
 *
 * The less secure method of doing so as it
 * won't ask to verify the user's password (or the credentials of the user
 * who is requesting the deletion of the account)
 *
 * @module usersController
 * @function
 * @param {Object} req - The request
 * @param {String} req.parmas.id - The id of the user to delete
 * @param {Object} res - The response
 * @return {Undefined}
 */
exports.deleteUser = (req, res) => {
    // For debugging
    console.log(`Deleting user "${req.params.id}"`);

    Users.findByIdAndDelete( req.params.id )
    .then( (result) => {
        // The user has been deleted jim!
        res.status(200).send({
            msg: `User "${req.params.id}" has been deleted`
        });
    })
    .catch( (err) => {
        // Whoops! Something stopped us from deleting the user!
        console.log(err);   // Log the error

        if( err.kind == 'ObjectId' ) {
            res.status(404).send({
                msg: `usersController.deleteUser | Could not find user with id "${req.params.id}"`
            });
        } else {
            res.status(500).send({
                msg: `usersController.deleteUser | There was a problem processing your request`,
                error: err.message
            });
        }
    });
};

/**
 * Delete a user from the users collection
 *
 * The slightly more secure method of doing so, whereby we also need the password
 * of the user to match before we actually delete the user!
 *
 * TODO - FUTURE - In the future, we should go one step further and check
 * the authorisation tags of the user requesting the deletion of the account
 *
 * @module usersController
 * @function
 * @param {Object} req - The request
 * @param {String} req.params.id - The id of the user to
 * @param {String} req.params.pw - The password of the user for verification
 * @param {Object} res - The response
 * @return {Undefined}
 */
exports.deleteUserVerified = (req, res) => {
    // For debugging
   console.log(`Deleting user "${req.params.id}" with password verification`);

    // Verify the password
    Users.verifyPassword( req.params.pw, (err, result) => {
        if(err) {
            // An error has occured
            console.log(err);
            res.status(500).send({
                msg: `usersController.deleteUserVerified | There was a problem processing your request`,
                error: err.message
            });
        } else {
            if(result) { // If password verification was successful
                // Start deleting the user (couldn't we just call deleteUser?)
                Users.findByIdandRemove( req.params.id )
                .then( (result) => {
                    // The user has been deleted jim!
                    res.status(200).send({
                        msg: `User "${req.params.id}" has been deleted`
                    });
                })
                .catch( (err) => {
                    // Whoops! Something stopped us from deleting the user!
                    console.log(err);   // Log the error
                    res.status(500).send({
                        msg: `usersController.deleteUser | There was a problem processing your request`,
                        error: err.message
                    });
                });
            } else { // If the password verification failed
                // Notify the user
                console.log('Was given incorrect credentials! Aborting User Deletion!!!');
                res.status(401).send({
                    msg: `usersController.deleteUser | Could not delete user "${req.params.id}"`,
                    reason: `Authentication / verification failure`
                });
            }
        }
    });
};
