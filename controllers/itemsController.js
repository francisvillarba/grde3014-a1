'use strict';

/**
 * itemsController.js.
 *
 * The controller that will respond with relevant data, based on routed info
 * from the users.
 *
 * Inspired by https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/routes
 *
 * @author  Francis Villarba <francis.villarba@student.curtin.edu.au>
 * @version 1.1.7
 *
 * @requires nodejs
 * @requires mongodb
 * @requires express
 * @requires mongoose
 */

// Setup -------------------------------------------------------------------- //
var Item = require('../models/item');

// Listing ------------------------------------------------------------------ //

/**
 * Obtains a list of all items in the mongoDB "items" collection.
 * @module itemsController
 * @function
 * @param {Object} req - The request, should be empty in this instance
 * @param {Object} res - The response to return to
 * @return {undefined}
 */
exports.getItems = (req, res) => {
    // For debugging
    console.log(`Getting all items from database`);

    // Run the mongoDB command
    Item.find({})
    // If successful, run this (Yay for JS Promises!)
    .then( (items) => {
        if( items.length == 0 ) {
            // If there is no items, send a 404 not found error JSON message
            res.status(404).send({
                msg: 'No items found'
            });
        } else {
            // Else send back json of books as a response
            res.json(items);
        }
    })
    // If this fails, do this
    .catch( (err) => {
        console.log(err);
        // To make it easier to debug, list the function where error occurs
        res.status(500).send({
            msg: 'itemsController.getItems | There was a problem processing your request',
            error: err.message
        });
    });
};

/**
 * Obtains information on a single item based on a given MongoDB Object_id
 * @module itemsController
 * @function
 * @param {Object} req - The request
 * @param {Object} res - The response
 * @param {String} req.params.id - The MongoDB Object_id
 * @return {Undefined}
 */
exports.getItem = (req, res) => {
    // For debugging
    console.log(`Getting Item "${req.params.id}"`);

    // Run the mongoDB command
    Item.findById(req.params.id)
    // If successful run this
    .then( (result) => {
        // If we found an item, send response
        res.json(result);
    })
    .catch( (err) => {
        // Log the error
        console.log(err);
        // Figure out a response to the user
        if( err.kind == 'ObjectId' ) {
            // If the error is related to the user supplied ObjectID
            res.status(404).send({
                msg: `Item of ID "${req.params.id}" was not found`
            });
        } else {
            // Some server related error has occured
            res.status(500).send({
                msg: `itemsController.getItem | There was a problem processing your request`,
                error: err.message
            });
        }
    });
};

// Creation ----------------------------------------------------------------- //

/** Create an item | Getting the creation form */
// TODO - FUTURE - Create an item | Getting the creation form
exports.createItemView = (req, res) => {
    console.log(`Call made to not implemented function | itemsController.createItemView`);
    res.status(501).send(`Not Implemented: itemsController.createItemView | ${req.params.id}`);
    // By using packages such as pug, one can use a template view document, setup
    // 1. Set the view engine to pug on server.js Middleware
    // 2. Create pug template file for itemCreate in view folder
    // do res.rende function with 'itemCreate' as first parameter, then pass required field variables
};

/**
 * Creates a new item document in the MongoDB items collection
 * @module itemsController
 * @function
 * @param {Object} req - The request
 * @param {Object} req.body - User suplied json with info about the new item
 * @param {Object} res - The response
 * @return {Undefined}
 */
exports.createItemPost = (req, res) => {
    // For Debugging
    console.log('Creating new item');

    // Valid the request
    if(!req.body) {
        console.log('Was given empty body! Aborting Item Creation!!!');
        res.status(400).send({
            msg: 'The item content cannot be empty!'
        });
    } else { // Create the item

        // For debugging
        // console.log(req.body);
        // res.send('body received');

        let newItem = new Item(req.body);
        newItem.save()
        .then( (item) => {
            res.status(201).json(item);
        })
        .catch( (err) => {
            // Log the error
            console.log(err);
            // Notify the user via response
            res.status(500).send({
                msg: `itemsController.createItemPost | There was a problem processing your request`,
                error: err.message
            })
        });
    }
};

// Editing ------------------------------------------------------------------ //

/** Edit an item | Getting the edit form */
// TODO - FUTURE - Edit an item | Getting the edit form
exports.editItemView = (req, res) => {
    console.log(`Call made to not implemented function | itemsController.editItemView`);
    res.status(501).send(`Not Implemented: itemsController.editItemView | ${req.params.id}`);
};

/**
 * Edits an item given by the item's MongoDB Object_id and json data
 *
 * !!!NOTE!!!
 * This function does not work with some aspects of the item schema,
 * such as the tags and altName string arrays. Use the addToArray
 * and pullFromArray functions instead.
 *
 * @module itemController
 * @function
 * @param {Object} req - The request
 * @param {String} req.params.id - The mongoDB Object_id of the item to edit
 * @param {Object} req.body - User supplied json with the new info to update item with
 * @param {Object} res - The response
 * @return {Undefined}
 */
exports.editItemPost = (req, res) => {
    // For Debugging
    console.log(`Editing Item "${req.params.id}"`);

    if(!req.body) {
        // If the body of the post is empty
        console.log('Was given empty body! Aborting Item Edit!!!');
        res.status(400).send({
            msg: 'The item content cannot be empty!'
        });
    } else { // Edit the item
        // Set the new values
        Item.findByIdAndUpdate( req.params.id, req.body, { new: true })
        .then( (result) => {
            // Print out the results (which is the updated document as new == true )
            res.json(result);
        }).catch( (err) => {
            // Log the error
            console.log(err);
            if(err.kind == 'ObjectId') {
                // If it is related to the objectId not being found
                res.status(404).send({
                    msg: 'itemsController.editItemPost | Could not find the specified item'
                });
            } else {
                // If it is any other error, assume server issue
                res.status(500).send({
                   msg: 'itemsController.editItemPost | There was a problem processing your request',
                   error: err.message
                });
            }
        });
    }
};

/**
 * Adds an item to an array such as tags and alternate names.
 * @module itemController
 * @function
 * @param {Object} req - The request
 * @param {String} req.params.id - The mongoDB Object_id of the item to edit
 * @param {Object} req.body - User supplied json with the new information to add
 * @param {Object} res - The response
 * @response {Undefined}
 */
exports.addToArray = (req, res) => {
    // For debugging
    console.log(`Editing Item "${req.params.id}" array data | Add`);

    if(!req.body) {
        console.log('Was given empty body! Aborting Item Edit!!!');
        // If the body of the post is empty
        res.status(400).send({
            msg: `The tag body cannot be empty!`
        });
    } else {// Let's update the tags array

        // For debugging
        // console.log(req.body);

        // Talk to MongoDB
        Item.findByIdAndUpdate(req.params.id, { $push: req.body }, { new: true })
        .then( (result) => {
            res.status(200).json(result);
        })
        .catch( (err) => {
            if(err.kind == 'ObjectId') {
                res.status(404).send({
                    msg: 'itemsController.addTag | Could not find the specified item'
                });
            } else {
                // If it is any other error, assume server issue
                res.status(500).send({
                   msg: 'itemsController.addTag | There was a problem processing your request',
                   error: err.message
                });
            }
        });
    }
};

/**
 * Removes a particular item's tag or alternate name by updating the relevant array
 * @module itemController
 * @function
 * @param {Object} req - The request
 * @param {String} req.params.id - The mongoDB Object_id of the item to edit
 * @param {Object} req.body - User supplied json with the new information to add
 * @param {Object} res - The response
 * @return {Undefined}
 */
exports.pullFromArray = (req, res) => {
    // For debugging
    console.log(`Editing Item "${req.params.id}" array data | Pull / Remove`);

    if(!req.body) {
        console.log('Was given empty body! Aborting Item Edit!!!');
        res.status(400).send({
            msg: `The tag body cannot be empty!`
        });
    } else { // Let's remove the tag from the array
        Item.findByIdAndUpdate(req.params.id, { $pull: req.body }, { new: true })
        .then( (result) => {
            // Output the result
            res.status(200).json(result);
        })
        .catch( (err) => {
            // Log the error to console
            console.log(err);

            // Figure out what the error is and give appropriate response
            if(err.kind == 'ObjectId') {
                res.status(404).send({
                    msg: 'itemsController.remTag | Could not find the specified item'
                });
            } else {
                // Other error, not caught by us
                res.status(500).send({
                    msg: 'itemController.remTag | There was a problem processing your request',
                    error: err.message
                });
            }
        });
    }
};

// Deletion ------------------------------------------------------------------ //

/* Getting the deletion form */
// TODO - FUTURE - | Getting the deletion form
exports.deleteItemGet = (req, res) => {
    /*
    * Technically speaking, it is not a great idea to delete items once they have been created
    * as it can cause problems for collections that link to these items, such as the order history.
    * But, for the purposes of this assignment, deletion will be allowed in order to fulfil
    * the submission / assessment criteria.
    *
    * An alternative method is to call for the orderController to check if any orders contains
    * the given Object_id, if no, deletion is possible, if yes, cannot delete and return an
    * error message with the HTTP status code, related to "Forbidden".
    */
    console.log(`Call made to not implemented function | itemsController.deleteItemGet`);
    res.status(501).send(`Not Implemented: itemsController.item_delete_get | ${req.params.id}`);
};

/**
 * Deletes an item from the mongoDB items collection
 * @module itemController
 * @function
 * @param {Object} req - The request
 * @param {String} req.params.id - The mongoDB Object_id of the item to delete
 * @param {Object} req.body - This should be blank, will be ignored if any given
 * @param {Object} res - The response
 * @return {Undefined}
 */
exports.deleteItem = (req, res) => {
    // For debugging
    console.log(`Deleting Item "${req.params.id}"`);

    // Run the mongoDB command promise
    Item.findByIdAndRemove(req.params.id)
    .then( (result) => {
        // Notify success as well as info that it is not recommended
        res.status(200).send({
            msg: `Item _id ${req.params.id} was successfully deleted`,
            note: `NOTE! It is generally not a good idea to delete items as it could potentially cause problems with the integrity of the order history`
        });
    })
    .catch( (err) => {
        // Log the error
        console.log(err);
        if(err.kind == 'ObjectId') {
            // We could not find the specified ObjectID to delete
            res.status(404).send({
                msg: `Could not find item with _id ${req.params.id}`
            });
        } else {
            // General Error Occured - Assume Server Error
            res.status(500).send({
                msg: `itemsController.deleteItem | Problem deleting a book`,
                error: err.message
            });
        }
    });
};
